/**
 * Copyright 2015 StreamSets Inc.
 * <p>
 * Licensed under the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.streamsets.stage.lib;

import com.streamsets.pipeline.api.ErrorCode;
import com.streamsets.pipeline.api.GenerateResourceBundle;

@GenerateResourceBundle
public enum Errors implements ErrorCode {

    //0XX error for initialization error or init file read error
    ERROR_001("Destination device not responding. check firewall settings, ping result, port is reachable."),
    ERROR_002("CSV file cannot be found as named in 'Device Name' Please verify the device name and filename, or file is located correctly"),
    ERROR_003("CSV file cannot be read while trying, check permission or file is corrupted.  {}"),
    ERROR_004("CSV encoding is incorrect check CSV encoding is EUC-KR or UTF-8.  {}"),
    ERROR_005("CSV file column is not compatible with designated. detail cause is : {}"),
    ERROR_006("CSV address range cannot be recongnized. check address range is in proper range.  {}"),
    ERROR_007("CSV Data type cannot be recongnized. check Data type is correct 'WORD','DWORD' or etc.  {}"),
    ERROR_008("Address List is empty, please verify the address tab"),
    ERROR_009("Failed to create MELSEC connector, check network connectivity"),
    ERROR_030("PLC Address bound error, check address is not input properly ex) D1F {}"),
    ERROR_010("Receiving TCP/IP Connection Has failed. Check Melsec TCP port is opened or ping command reachable{}"),
    ERROR_011("TCP/IP Connection has established, However the TCP cannot get send Message.{}"),
    ERROR_012("Read Points for one time exceeds maximum read points. Default MAX_POINT_READ IS {}, set points less than {}"),
    ERROR_020("Sending TCP/IP Connection Has failed. Check Melsec TCP port is opened or ping command reachable{}"),
    ERROR_021("TCP/IP Connection has established, However the TCP cannot get send Message.{}"),

    ERROR_050("IP Address blank, Input Ip Address"),
    ERROR_051("Address name is blank, Input Proper value"),
    ERROR_052("DataType name is blank, Input Proper value"),
    ERROR_053("Value name is blank, Input Proper value"),
    ERROR_055("IP address is not input properly, check ip address format"),
    ERROR_056("Redundancy address is not input properly, check redundancy address is empty"),
    //1XX for the sending command errors.
    ERROR_100("Failed to send reply message, check address or syntax is correct"),
    ERROR_101("No reply from PLC device, check the connectivity"),
    ERROR_102("Syntax error for reply message. Numeric message only support Short Integer(0~65535 or -32768-32767)"),
    ERROR_103("Address range incorrect for reply message. check the address is supporting only True/false or 1/0"),
    ERROR_105("No reply from PLC device, Retry port changed into {}, current message is discarded!!"),
    ERROR_110("Failed to send boolean message check address is correct"),
    ERROR_120("Failed to send word message check address is correct"),
    ERROR_130("Failed to send Dword message check address is correct"),
    ERROR_140("Failed to send Qword message check address is correct"),
    ERROR_150("Record format error {}"),
    ERROR_151("Sleep interrupt failed {}"),
    ERROR_152("IP Address is empty(null), check ipAddress is empty int the '/ipAdderss' if you choose dynamic ipaddress, '/ipAddress' should be exist"),
    ERROR_153("Address is empty(null)"),
    ERROR_154("Value is empty(null)"),
    ERROR_155("Improper dataType or null. dataType should either {BOOLEAN|WORD|DWORD|QWORD|STRING|UNSIGNED_INTEGER|SIGNED_INTEGER|LONG|DOUBLE|FLOAT} as per dataType tagName"),
    //2XX for receiving command errors;
    ERROR_200("Check Address should fit in DEC : {}"),
    ERROR_201("Verify Data read type : {}"),

    //3xx error for COM port errors

    //4XX error for UDP connection
    //5xx error for tcp connection error
    ERROR_501("TCP/IP Connection has established, However the TCP cannot get send Message.{}"),




    ERROR_600("System Type cannot verified, for building address binary check System Type. Some model is not implemented!{}"),
    ERROR_601("System Type cannot verified for building command binary, check System Type. Some model is not implemented!{}"),
    ERROR_603("System Type cannot verified for dataType, check System Type. Some model is not implemented!{}"),
    ERROR_604("System Type cannot verified for building CPU Location, check System Type. Some model is not implemented!{}"),
    ERROR_605("Register Tag cannot verified during building the result address{}"),


    ERROR_700("Attempted Disconnection failed. details : {}"),
    ERROR_701("Scan rate is exceeding the maximum scan rate limit, check PLC is working.");
    private final String msg;

    Errors(String msg) {
        this.msg = msg;
    }

    /** {@inheritDoc} */
    @Override
    public String getCode() {
        return name();
    }

    /** {@inheritDoc} */
    @Override
    public String getMessage() {
        return msg;
    }
}
