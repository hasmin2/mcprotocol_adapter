package com.streamsets.stage.lib.Connector;

import com.opencsv.CSVReader;
import com.streamsets.pipeline.api.StageException;
import com.streamsets.stage.lib.Errors;
import com.streamsets.stage.lib.MelsecConstants;
import com.streamsets.stage.lib.menus.CustomDataListMenu;
import com.streamsets.stage.lib.menus.OutputDataType;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Pattern;

public class KepwareCSVReader implements IOListFileReader {
    private CSVReader reader;

    @Override
    public void init(String fileName) throws StageException {
        try {
            reader = new CSVReader(new InputStreamReader(new FileInputStream(fileName), StandardCharsets.UTF_8));
            String[] columnLabel = reader.readNext();
            if (columnLabel.length != MelsecConstants.KEPWARE_CSV_COLUMN) {
                throw new StageException(Errors.ERROR_005, "Expected column : " + MelsecConstants.KEPWARE_CSV_COLUMN + "Actual Column : " + columnLabel.length);
            }

        } catch (UnsupportedEncodingException e) {
            throw new StageException(Errors.ERROR_004, e.getMessage());
        } catch (FileNotFoundException e) {
            throw new StageException(Errors.ERROR_002, e.getMessage());
        } catch (IOException e) {
            throw new StageException(Errors.ERROR_003, e.getMessage());
        }
    }

    @Override
    public List<CustomDataListMenu> fileRead() throws StageException {
        List<CustomDataListMenu> resultList = new ArrayList<>();
        TreeMap<String, String []>  sortingMap = new TreeMap<>();
        try {
            String[] columnValue;
            while ((columnValue = reader.readNext()) != null) {
                String realAddress = extractAddress(columnValue[1]);
                sortingMap.put(realAddress, columnValue);
            }
            Iterator<String> it = sortingMap.keySet().iterator();
            String lastAddress="";
            String firstAddress = "";
            int points=1;
            String lastLowScale = "";
            String lastHiScale = "";
            String lastDataType="";
            String [] eachLine;
            while (it.hasNext()){
                eachLine = sortingMap.get(it.next());
                if(isNextAddress(extractAddress(eachLine[1]), lastAddress, eachLine[2]) && isSameScale(lastLowScale, lastHiScale, eachLine[8], eachLine[10]) ){
                    points++;
                    if(points>=MelsecConstants.READ_POINT_LIMIT){
                        resultList.add(insertAddressList(firstAddress, points, lastLowScale, lastHiScale, lastDataType));
                        firstAddress = eachLine[1];
                        lastLowScale = eachLine[8];
                        lastHiScale = eachLine[10];
                        lastDataType = eachLine[2];
                        points=1;
                    }
                }
                else {
                    if(firstAddress.equals("")){
                        firstAddress = eachLine[1];
                    }
                    else {
                        resultList.add(insertAddressList(firstAddress, points, lastLowScale, lastHiScale, lastDataType));
                        firstAddress = eachLine[1];
                        lastLowScale = eachLine[8];
                        lastHiScale = eachLine[10];
                        lastDataType = eachLine[2];
                    }
                    points=1;
                }
                lastAddress = eachLine[1];
            }
            resultList.add(insertAddressList(firstAddress, points, lastLowScale, lastHiScale, lastDataType));
        }
        catch (IOException e) {
            throw new StageException(Errors.ERROR_003, e.getMessage());
        }
        catch (NullPointerException ne){ throw new StageException(Errors.ERROR_005); }

        return resultList;
    }
    private CustomDataListMenu insertAddressList(String firstAddress, int points, String lastLowScale, String lastHiScale, String lastDataType){
        CustomDataListMenu customDataList = new CustomDataListMenu();
        customDataList.startAddress=firstAddress;
        customDataList.points = points;
        if (lastHiScale.equals("") || lastLowScale.equals("")) { customDataList.scale = MelsecConstants.SCALE_FACTOR_BASE; }
        else { customDataList.scale = Integer.parseInt(lastHiScale.split("\\.")[0]) * (int) MelsecConstants.SCALE_FACTOR_BASE / Integer.parseInt(lastLowScale.split("\\.")[0]); }
        if (lastDataType.equalsIgnoreCase("QWORD")) { customDataList.dataType = OutputDataType.QWORD; }
        else if (lastDataType.equalsIgnoreCase("DWORD")) { customDataList.dataType = OutputDataType.DWORD; }
        else if (lastDataType.equalsIgnoreCase("LONG")) { customDataList.dataType = OutputDataType.LONG; }
        else if (lastDataType.equalsIgnoreCase("FLOAT")) { customDataList.dataType = OutputDataType.FLOAT; }
        else if (lastDataType.equalsIgnoreCase("WORD")) {customDataList.dataType = OutputDataType.WORD; }
        else if (lastDataType.equalsIgnoreCase("SHORT")) { customDataList.dataType = OutputDataType.SIGNED_INTEGER; }
        else if (lastDataType.equalsIgnoreCase("INTEGER")) { customDataList.dataType = OutputDataType.SIGNED_INTEGER; }
        else if (lastDataType.equalsIgnoreCase("BOOLEAN")) { customDataList.dataType = OutputDataType.BOOLEAN; }
        return customDataList;
    }
    private boolean isSameScale(@NotNull String lastLowScale, String lastHiScale, String s, String s1) {
        boolean result = false;
        if(lastLowScale.equals(s) && lastHiScale.equals(s1)){result = true;}
        return result;
    }


    private boolean isNextAddress(String address, @NotNull String lastAddress, String dataType) {
        boolean result = false;
        if(lastAddress.length()>0) {
            if (address.charAt(0) == lastAddress.charAt(0)) {
                if (dataType.equalsIgnoreCase("QWORD") || dataType.equalsIgnoreCase("DOUBLE")) {
                    if (address.contains(increaseAddress(lastAddress, isHex(lastAddress), 4))) {
                        result = true;
                    }
                } else if (dataType.equalsIgnoreCase("DWORD") || dataType.equalsIgnoreCase("FLOAT") || dataType.equalsIgnoreCase("LONG")) {
                    if (address.contains(increaseAddress(lastAddress, isHex(lastAddress), 2))) {
                        result = true;
                    }
                } else {
                    if (address.contains(increaseAddress(lastAddress, isHex(lastAddress), 1))) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    private boolean isHex(@NotNull String prefix) {
        boolean result;
        String item = prefix.toUpperCase();
        result = item.startsWith(MelsecConstants.X_REG_PREFIX) || item.startsWith(MelsecConstants.Y_REG_PREFIX) || item.startsWith(MelsecConstants.B_REG_PREFIX) || item.startsWith(MelsecConstants.W_REG_PREFIX);
        return result;
    }

    @NotNull
    private String increaseAddress(String currentAddress, boolean isHex, int step) {
        long addressNext;
        StringBuilder result;
        if (isHex) {
            addressNext = Long.parseLong(extractNumberOnly(currentAddress), 16);
            addressNext = addressNext + step;
            result = new StringBuilder(Long.toHexString(addressNext));
        }
        else {
            addressNext = Long.parseLong(extractNumberOnly(currentAddress));
            addressNext = addressNext + step;
            result = new StringBuilder(String.valueOf(addressNext));
        }
        while (result.length() < 6) { result.insert(0, "0"); }
        return result.toString().toUpperCase();
    }
    private String extractAddress(String address) {
        String addressPrefix;
        String addressNumber;
        if(Pattern.matches("[A-Z,a-z]{2}\\w*", address)){
            addressPrefix = address.substring(0,2);
            addressNumber = getAddressNumber(address.substring(2));
        }
        else{
            addressPrefix = address.substring(0,1);
            addressNumber = getAddressNumber(address.substring(1));
        }
        return addressPrefix+addressNumber;
    }
    private String extractNumberOnly(String address) {
        String addressNumber;
        if(Pattern.matches("[A-Z,a-z]{2}\\w*", address)){
            addressNumber = getAddressNumber(address.substring(2));
        }
        else{
            addressNumber = getAddressNumber(address.substring(1));
        }
        return addressNumber;
    }

    private String getAddressNumber(String inputString) {
        StringBuilder inputStringBuilder = new StringBuilder(inputString);
        while (inputStringBuilder.length()<6){ inputStringBuilder.insert(0, "0"); }
        inputString = inputStringBuilder.toString();
        return inputString;
    }
}
