package com.streamsets.stage.lib.menus;

import com.streamsets.pipeline.api.GenerateResourceBundle;
import com.streamsets.pipeline.api.Label;
import com.streamsets.stage.lib.MelsecConstants;

@GenerateResourceBundle
public enum InputDataFormat implements Label {
    MULTIPLEJSON(MelsecConstants.OPTION_VALUE_MULTIPLE_JSON_FORMAT),
    JSONARRAY(MelsecConstants.OPTION_VALUE_JSON_ARRAY_FORMAT);

    private final String label;

    InputDataFormat(String label) { this.label = label; }

    @Override
    public String getLabel() {
        return label;
    }
}
