package com.streamsets.stage.lib.Connector;

import com.streamsets.pipeline.api.StageException;
import com.streamsets.stage.lib.menus.CustomDataListMenu;

import java.util.List;

public interface IOListFileReader {
    void init(String fileName) throws StageException;

    List<CustomDataListMenu> fileRead() throws StageException;
}
