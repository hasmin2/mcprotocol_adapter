package com.streamsets.stage.lib.client;


import com.streamsets.stage.lib.core.MelsecClientOptions;
import com.streamsets.stage.lib.core.message.Function;
import com.streamsets.stage.lib.core.message.e.Frame3EBinaryCommand;
import com.streamsets.stage.lib.core.message.e.FrameEResponse;
import com.streamsets.stage.lib.melsec.codec.ClientFrame3EBinaryMessageDecoder;
import com.streamsets.stage.lib.melsec.codec.ClientFrameEMessageEncoder;
import com.streamsets.stage.lib.melsec.codec.Frame3EBinaryByteDecoder;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelPipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CompletableFuture;

/**
 * @author liumin
 */
public class Melsec3EBinaryEthClient extends AbstractEthClient {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    Melsec3EBinaryEthClient(MelsecClientConfig config) {
        super(config);
    }

    @Override
    protected void initChannel(ChannelPipeline pipeline) {
        pipeline.addLast(ClientFrameEMessageEncoder.INSTANCE);
        pipeline.addLast(new Frame3EBinaryByteDecoder());
        pipeline.addLast(new ClientFrame3EBinaryMessageDecoder());
    }

    @Override
    public CompletableFuture<ByteBuf> batchRead(String address, int points) {
        CompletableFuture<ByteBuf> future = new CompletableFuture<>();
        CompletableFuture<FrameEResponse> response = sendRequest(new Frame3EBinaryCommand(
                Function.BATCH_READ,
                address,
                points,
                new MelsecClientOptions(config.getNetworkNo(),
                        config.getPcNo(),
                        config.getRequestDestinationModuleIoNo(),
                        config.getRequestDestinationModuleStationNo())));
        response.whenComplete((r, ex) -> {
            if (r != null) {
                future.complete(r.getData());
            } else {
                future.completeExceptionally(ex);
            }
        });

        return future;
    }

    @Override
    public CompletableFuture<Void> batchWrite(String address, int points, ByteBuf data) {
        CompletableFuture<Void> future = new CompletableFuture<>();
        CompletableFuture<FrameEResponse> response = sendRequest(new Frame3EBinaryCommand(
                Function.BATCH_WRITE,
                address,
                points,
                data,
                new MelsecClientOptions(config.getNetworkNo(),
                        config.getPcNo(),
                        config.getRequestDestinationModuleIoNo(),
                        config.getRequestDestinationModuleStationNo())));
        response.whenComplete((r, ex) -> {
            if (r != null) {
                future.complete(null);
            } else {
                future.completeExceptionally(ex);
            }
        });
        return future;
    }
}
