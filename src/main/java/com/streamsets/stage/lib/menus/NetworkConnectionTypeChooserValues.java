package com.streamsets.stage.lib.menus;

import com.streamsets.pipeline.api.base.BaseEnumChooserValues;

public class NetworkConnectionTypeChooserValues extends BaseEnumChooserValues <NetworkConnectionType> {
    public NetworkConnectionTypeChooserValues() { super(NetworkConnectionType.class); }
}
