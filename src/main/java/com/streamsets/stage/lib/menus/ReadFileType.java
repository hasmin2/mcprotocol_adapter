package com.streamsets.stage.lib.menus;

import com.streamsets.pipeline.api.Label;
import com.streamsets.stage.lib.MelsecConstants;

public enum ReadFileType implements Label {
    KEPWARECSV(MelsecConstants.OPTION_VALUE_KEPWARE_CSV),
    LISTFROMUI(MelsecConstants.OPTION_VALUE_LIST_FROM_UI);
    //BUILT("More implemets if required");

    private final String label;

    ReadFileType(String label) {
        this.label = label;
    }

    @Override
    public String getLabel() {
        return label;
    }
}
