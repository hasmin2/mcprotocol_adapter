package com.streamsets.stage.lib.menus;

import com.streamsets.pipeline.api.base.BaseEnumChooserValues;

public class NetworkTypeChooserValues extends BaseEnumChooserValues<NetworkType> {
    public NetworkTypeChooserValues() {
        super(NetworkType.class);
    }
}