/**
 * Copyright 2015 StreamSets Inc.
 * <p>
 * Licensed under the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.streamsets.stage.origin;

import com.streamsets.pipeline.api.BatchMaker;
import com.streamsets.pipeline.api.Field;
import com.streamsets.pipeline.api.Record;
import com.streamsets.pipeline.api.StageException;
import com.streamsets.pipeline.api.base.BaseSource;
import com.streamsets.stage.lib.Connector.IOListFileReader;
import com.streamsets.stage.lib.Connector.KepwareCSVReader;
import com.streamsets.stage.lib.Errors;
import com.streamsets.stage.lib.MelsecConstants;
import com.streamsets.stage.lib.client.MelsecClient;
import com.streamsets.stage.lib.client.MelsecClientConfig;
import com.streamsets.stage.lib.menus.CustomDataListMenu;
import com.streamsets.stage.lib.menus.TransferModeSelect;
import com.streamsets.stage.lib.util.AddressListGenerator;
import com.streamsets.stage.lib.util.AddressListStore;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.Socket;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static java.lang.Thread.sleep;

/**
 * This source is an example and does not actually read from anywhere.
 * It does however, generate generate a simple record with one field.
 */
public abstract class McprotocolSource extends BaseSource {
    /**
     * Gives access to the UI configuration of the stage provided by the {@link McprotocolDSource} class.
     */
    public abstract boolean isRedundancy();

    public abstract int getAddr_Cycle_Timeout();

    public abstract List<String> getRedundancyAddressList();
    public abstract Map<String,String>getReplyValueMap();
    public abstract String getNetworkType();
    public abstract String getIPAddress();
    public abstract int getPort();
    public abstract String getComPort();
    public abstract String getFilePathName();
    public abstract int getScanRate();
    public abstract TransferModeSelect getTransferMode();
    public abstract String getCustomDataListFromCsv();
    public abstract String getBooleanOutputType();
    public abstract List<CustomDataListMenu> getCustomDataListMenus();
    public abstract String getNetworkConnectionType();
    public abstract int getMelsecStationNumber();
    //PC 쪽에 서지정하 는plc 번 호리턴
    public abstract int getPcNetworkNumber();
    public abstract int getPcStationNumber();
    public abstract int getMelsecCPULocation();
    public abstract String getProtocolType();
    private int scanRate;
    private Map <String, Object> lastBoolIntMap = new HashMap<>();
    private Map <String, String> lastStringMap = new HashMap<>();
    private Map <String, Double> lastNumberMap = new HashMap<>();
    private List<AddressListStore> addressList;
    private List<String> redundancyAddress;
    private int transferMode = 0;
    private boolean isReply = false;
    private Map<String, String> replyMessageMap;
    private int currentPortIndex=0;
    private int redundancyAddressSize;
    private MelsecClient client;
    private boolean isBoolOut = false;
    private boolean isAscii = false;
    private boolean exceptionOccurred;
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private void initClient(int port){
        MelsecClientConfig config = new MelsecClientConfig.Builder(getIPAddress())
                .setConnectionType(getNetworkConnectionType())
                .setNetworkNo(getPcNetworkNumber())
                .setPcNo(getPcStationNumber())
                .setRequestDestinationModuleStationNo(getMelsecCPULocation())
                .setRequestDestinationModuleStationNo(getMelsecStationNumber())
                .setTimeout(Duration.ofMillis(getAddr_Cycle_Timeout()))
                .setPort(port).build();
        if(getProtocolType().equals(MelsecConstants.OPTION_VALUE_BINARY)) { client = MelsecClient.create3EEthBinary(config); }
        else { client = MelsecClient.create3EEthAscii(config); }
    }
    @Override
    protected List<ConfigIssue> init() {
        if(getTransferMode().name().equalsIgnoreCase("ONLYCHANGE")){ transferMode = 0; }
        else if (getTransferMode().name().equalsIgnoreCase("WHILEONLINE")){ transferMode = 1; }
        else{ transferMode = 2;}
        scanRate = getScanRate();
        if(getBooleanOutputType().equals(MelsecConstants.OPTION_VALUE_BOOLEAN_TYPE)){ isBoolOut = true; }
        if(getProtocolType().equals(MelsecConstants.OPTION_VALUE_ASCII)){ isAscii = true; }
        List<ConfigIssue> issues = super.init();
        if(isRedundancy()){
            redundancyAddress = new ArrayList<>();
            redundancyAddress.add(String.valueOf(getPort()));
            redundancyAddress.addAll(getRedundancyAddressList());
            redundancyAddressSize = redundancyAddress.size();
        }
        initClient(getPort());
        if(getCustomDataListFromCsv().equals(MelsecConstants.OPTION_VALUE_LIST_FROM_UI)) {
            addressList = new AddressListGenerator(getCustomDataListMenus()).getAddressList();
        }
        else{
            IOListFileReader kepReader = new KepwareCSVReader();
            kepReader.init(getFilePathName());
            addressList = new AddressListGenerator(kepReader.fileRead()).getAddressList();
        }
        for (AddressListStore addressListStore : addressList) {
            if (addressListStore.getPoints() > MelsecConstants.READ_POINT_LIMIT) {
                issues.add(getContext().createConfigIssue(Groups.ADDRESS.name(), MelsecConstants.FILEPATH_LABEL, Errors.ERROR_012, "Current Read Points are" + addressListStore.getPoints(), MelsecConstants.READ_POINT_LIMIT));
            }
        }
        if(addressList == null|| addressList.size()==0){
            issues.add(getContext().createConfigIssue(Groups.ADDRESS.name(), MelsecConstants.DATA_MANAGE_SELECTION_LABEL, Errors.ERROR_008, "List Empty"));
        } else {
            addressList.stream().map(AddressListStore::getAddress).forEach(address -> {
                String prefix = getPrefix(address);
                try { getNumericAddress(address, prefix); }
                catch (Exception e) { issues.add(getContext().createConfigIssue(Groups.ADDRESS.name(), MelsecConstants.START_ADDRESS_LABEL, Errors.ERROR_030, MelsecConstants.NUMBER_FORMAT_ERROR_MSG));
                }});
        }
        if(client == null){
            issues.add(getContext().createConfigIssue(Groups.COMMUNICATION.name(), MelsecConstants.IP_ADDR_LABEL, Errors.ERROR_009, "MC protocol fail"));
        }
        if(getNetworkConnectionType().equals(MelsecConstants.OPTION_VALUE_TCPIP) && !isRedundancy()) {
            if (!checkConnectivity()) {
                issues.add(getContext().createConfigIssue(Groups.COMMUNICATION.name(), MelsecConstants.IP_ADDR_LABEL, Errors.ERROR_010, checkConnectivity()));
            }
        }
        if(!getReplyValueMap().isEmpty()) {
            try {
                replyMessageMap = getReplyValueMap();
                for (String key : replyMessageMap.keySet()) {
                    if (isAddressWordOrBit(key)) { Integer.parseInt(replyMessageMap.get(key)); }
                    else {
                        if (replyMessageMap.get(key).equalsIgnoreCase("TRUE") || replyMessageMap.get(key).equals("1")) { replyMessageMap.put(key, "true"); }
                        else if (replyMessageMap.get(key).equalsIgnoreCase("FALSE") || replyMessageMap.get(key).equals("0")) { replyMessageMap.put(key, "false"); }
                        else { issues.add(getContext().createConfigIssue(Groups.ADDRESS.name(), MelsecConstants.REPLY_MSG_LABEL, Errors.ERROR_103, "Reply message Syntax error")); }
                    }
                }
            }
            catch (NumberFormatException ne){ issues.add(getContext().createConfigIssue(Groups.ADDRESS.name(), MelsecConstants.REPLY_MSG_LABEL, Errors.ERROR_102, "Reply message Syntax error")); }
            isReply = true;
        }
        try {
            sleep((int) (Math.random() * 15000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return issues;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {
        disconnectClient();
        super.destroy();
    }
    private void disconnectClient(){
        if(client!=null){
            try { client.disconnect().get(getAddr_Cycle_Timeout(), TimeUnit.MILLISECONDS); }
            catch (InterruptedException | ExecutionException e) { throw new StageException(Errors.ERROR_700, e.getMessage());}
            catch (TimeoutException e) { throw new StageException(Errors.ERROR_700, e.toString()); }
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String produce(String lastSourceOffset, int maxBatchSize, BatchMaker batchMaker) throws StageException {
        long nextSourceOffset = 0;
        exceptionOccurred = false;
        if (lastSourceOffset != null) { nextSourceOffset = Long.parseLong(lastSourceOffset); }
        long startTime = System.currentTimeMillis();
        List<CompletableFuture<Void>> resultFinishList = new ArrayList<>();
        for (AddressListStore item : addressList) {
            String address = item.getAddress().toUpperCase();
            List<Object> result;
            CompletableFuture<Void> eachResult;
            if (isAddressWordOrBit(address)){
                switch (item.getDataType().name()) {
                    case MelsecConstants.UNSIGNED_INTEGER_CASE:
                    case MelsecConstants.SIGNED_INTEGER_CASE:
                        result = sendWordNumberData(item, batchMaker, nextSourceOffset);
                        nextSourceOffset = (long) result.get(0);
                        eachResult = (CompletableFuture<Void>) result.get(1);
                        resultFinishList.add(eachResult);
                        break;
                    case MelsecConstants.WORD_CASE:
                    case MelsecConstants.BOOLEAN_CASE:
                        result = sendWordNotNumberData(item, batchMaker, nextSourceOffset);
                        nextSourceOffset = (long) result.get(0);
                        eachResult = (CompletableFuture<Void>) result.get(1);
                        resultFinishList.add(eachResult);
                        break;
                    case MelsecConstants.STRING_CASE:
                        result = sendStringData(item, batchMaker, nextSourceOffset);
                        nextSourceOffset = (long) result.get(0);
                        eachResult = (CompletableFuture<Void>) result.get(1);
                        resultFinishList.add(eachResult);
                        break;
                    case MelsecConstants.DWORD_CASE:
                    case MelsecConstants.LONG_CASE:
                        result = sendWordsData(item,2, batchMaker, nextSourceOffset);
                        nextSourceOffset = (long) result.get(0);
                        eachResult = (CompletableFuture<Void>) result.get(1);
                        resultFinishList.add(eachResult);
                        break;
                    case MelsecConstants.DOUBLE_CASE:
                        result = sendRealNumberData(item,4, batchMaker, nextSourceOffset);
                        nextSourceOffset = (long) result.get(0);
                        eachResult = (CompletableFuture<Void>) result.get(1);
                        resultFinishList.add(eachResult);
                        break;
                    case MelsecConstants.FLOAT_CASE:
                        result = sendRealNumberData(item,2, batchMaker, nextSourceOffset);
                        nextSourceOffset = (long) result.get(0);
                        eachResult = (CompletableFuture<Void>) result.get(1);
                        resultFinishList.add(eachResult);
                        break;
                    case MelsecConstants.QWORD_CASE:
                        result = sendWordsData(item, 4,batchMaker, nextSourceOffset);
                        nextSourceOffset = (long) result.get(0);
                        eachResult = (CompletableFuture<Void>) result.get(1);
                        resultFinishList.add(eachResult);
                        break;
                    default:
                        throw new StageException(Errors.ERROR_201,getContext());
                }
            }
            else {
                result = sendBooleanData(item, batchMaker, nextSourceOffset);
                nextSourceOffset = (long) result.get(0);
                eachResult = (CompletableFuture<Void>) result.get(1);
                resultFinishList.add(eachResult);
            }
        }
        if(isReply) { resultFinishList.addAll(sendReplyMessage()); }
        try {
            CompletableFuture.allOf((resultFinishList.toArray(new CompletableFuture[0]))).join();
        } catch(Exception e){
            System.out.println(e.toString());
        }
        if(exceptionOccurred){
            disconnectClient();
            cycletoNextport();
        }
        long endTime = System.currentTimeMillis();
        long interval = getInterval(startTime, endTime);

        try { sleep(interval); }
        catch (InterruptedException ignored) { }
        return String.valueOf(nextSourceOffset);
    }
    private boolean isAddressWordOrBit(String address){
        boolean result;
        address = address.toUpperCase();
        result = address.startsWith("CN")
                || address.startsWith("SN")
                || address.startsWith("TN")
                || address.startsWith("W")
                || address.startsWith("D")
                || address.startsWith("R")
                || address.startsWith("Z");
        return result;
    }
    private long getInterval (long startTime, long endTime){
        long elaspedTime = (endTime - startTime);
        long interval = scanRate - elaspedTime;
        return interval > 0 ? interval : 0;
    }

    @NotNull
    private List<Object> sendBooleanData (@NotNull AddressListStore item, BatchMaker batchMaker, long nextSourceOffset){
        String address = item.getAddress();
        int points = item.getPoints();
        String prefix = getPrefix(address);
        int addressInt = getNumericAddress(address, prefix);
        CompletableFuture<Void> resultFuture = client.batchRead(address, points).thenAccept(r -> {
            int addressCount = addressInt;
            for (int i = 0; i < points; i++) {
                Object result;
                if(isBoolOut) { result = r.readBoolean(); }
                else { result = r.readBoolean() ? 1:0 ;}
                String eachAddress = prefix + setRadix(prefix, addressCount);
                if (transferMode==0) {
                    if (isDuplicateValue(eachAddress, result)) { sendRecord(eachAddress, result, batchMaker, nextSourceOffset); }
                }
                else {
                    storeLastValue(eachAddress, result);
                    sendRecord(eachAddress, result, batchMaker, nextSourceOffset);
                }
                addressCount++;
            }
            r.release();
        }).exceptionally(ex -> {
            if(transferMode==2) {
                printDisconnectMessage();
                int addressCount = addressInt;
                String eachAddress = prefix + setRadix(prefix, addressCount);
                for (int i = 0; i < points; i++) {
                    Object result = lastBoolIntMap.get(eachAddress);
                    if (result != null) { sendRecord(eachAddress, result, batchMaker, nextSourceOffset); }
                    addressCount++;
                }
            }
            else { printErrorMessage(ex); }
            return null;
        });
        return setReturnValue(nextSourceOffset + points, resultFuture);
    }

    @NotNull
    private List<Object> sendWordNumberData (@NotNull AddressListStore item, BatchMaker batchMaker, long nextSourceOffset){
        String address = item.getAddress().toUpperCase();
        String prefix = getPrefix(address);
        final int points = item.getPoints();
        int addressInt = getNumericAddress(address, prefix);
        String dataType = item.getDataType().name();
        CompletableFuture<Void> resultFuture = client.batchRead(address, points).thenAccept(r -> {
            int addressCount = addressInt;
            for (int i = 0; i < points; i++) {
                double result;
                if(dataType.equalsIgnoreCase(MelsecConstants.UNSIGNED_INTEGER_CASE)){
                    if(isAscii) { result = r.readUnsignedShortLE()*item.getScale(); }
                    else { result = r.readUnsignedShort()*item.getScale(); }
                }
                else {
                    if(isAscii) { result = r.readShortLE()*item.getScale(); }
                    else { result = r.readShort()*item.getScale(); }
                }
                String eachAddress = prefix + setRadix(prefix, addressCount);
                if (transferMode==0) {
                    if (isDuplicateValue(eachAddress, result)) { sendRecord(eachAddress, result, batchMaker, nextSourceOffset); }
                }
                else {
                    storeLastValue(eachAddress, result);
                    sendRecord(eachAddress, result, batchMaker, nextSourceOffset);
                }
                addressCount++;
            }
            r.release();
        }).exceptionally(ex-> {
            if(transferMode==2) {
                printDisconnectMessage();
                int addressCount = addressInt;
                for (int i = 0; i < points; i++) {
                    String eachAddress = prefix + setRadix(prefix, addressCount);
                    Double result = lastNumberMap.get(eachAddress);
                    if (result != null) { sendRecord(eachAddress, result, batchMaker, nextSourceOffset); }
                    addressCount++;
                }
            }
            else { printErrorMessage(ex); }
            return null;
        });
        return setReturnValue(nextSourceOffset + points, resultFuture);
    }

    @NotNull
    private List<Object> sendWordNotNumberData (@NotNull AddressListStore item, BatchMaker batchMaker, long nextSourceOffset){
        String address = item.getAddress().toUpperCase();
        String prefix = getPrefix(address);
        final int points = item.getPoints();
        int addressInt = getNumericAddress(address, prefix);
        String dataType = item.getDataType().name();
        CompletableFuture<Void> resultFuture;
        if(dataType.equals(MelsecConstants.BOOLEAN_CASE)){
            resultFuture = client.batchRead(address, points).thenAccept(r -> {
                int addressCount = addressInt;
                for (int i = 0; i < points; i++) {
                    int result;
                    if(isAscii) { result = r.readUnsignedShortLE(); }
                    else { result = r.readUnsignedShort(); }
                    String eachAddress = prefix + setRadix(prefix, addressCount);
                    for (int j = 0; j < 16; j++) {
                        Object objResult;
                        if(isBoolOut) { objResult = ((result << 31 - j) >>> 31) == 1; }
                        else {objResult = ((result << 31 - j) >>> 31);}
                        String boolAddress = eachAddress + "_" + j;
                        if (transferMode==0) {
                            if (isDuplicateValue(boolAddress, objResult)) { sendRecord(boolAddress, objResult, batchMaker, nextSourceOffset); }
                        }
                        else {
                            storeLastValue(boolAddress, objResult);
                            sendRecord(boolAddress, objResult, batchMaker, nextSourceOffset);
                        }
                    }
                    addressCount++;
                }
                r.release();
            }).exceptionally(ex -> {
                printDisconnectMessage();
                if(transferMode==2) {
                    printDisconnectMessage();
                    int addressCount = addressInt;
                    for (int i = 0; i < points; i++) {
                        String eachAddress = prefix + setRadix(prefix, addressCount);
                        for (int j = 0; j < 16; j++) {
                            String boolAddress = eachAddress + "_" + j;
                            Object result  = lastBoolIntMap.get(boolAddress);
                            if(result !=null) {sendRecord(boolAddress, result, batchMaker, nextSourceOffset); }
                            addressCount++;
                        }
                    }
                }
                else { printErrorMessage(ex); }
                return null;
            });
        }
        else {
            resultFuture = client.batchRead(address, points).thenAccept(r -> {
                int addressCount = addressInt;
                StringBuilder result= new StringBuilder();
                for (int i = 0; i < points; i++) {
                    String eachAddress = prefix + setRadix(prefix, addressCount);
                    char hiChar, lowChar;
                    if (isAscii) {
                        hiChar = (char) r.readByte();
                        lowChar = (char) r.readByte();
                    } else {
                        lowChar = (char) r.readByte();
                        hiChar = (char) r.readByte();
                    }
                    result.append(hiChar).append(lowChar);
                    if (transferMode==0) {
                        if (isDuplicateValue(eachAddress, result)) { sendRecord(eachAddress, result, batchMaker, nextSourceOffset); }
                    }
                    else {
                        storeLastValue(eachAddress, result.toString());
                        sendRecord(eachAddress, result.toString(), batchMaker, nextSourceOffset);
                    }
                    addressCount++;
                }
                r.release();
            }).exceptionally(ex-> {
                printDisconnectMessage();
                if(transferMode==2) {
                    printDisconnectMessage();
                    int addressCount = addressInt;
                    for (int i = 0; i < points; i++) {
                        String eachAddress = prefix + setRadix(prefix, addressCount);
                        String result = lastStringMap.get(eachAddress);
                        if(result !=null) { sendRecord(eachAddress, result, batchMaker, nextSourceOffset); }
                        addressCount++;
                    }
                }
                else { printErrorMessage(ex); }
                return null;
            });
        }
        return setReturnValue(nextSourceOffset + points, resultFuture);
    }
    private void printDisconnectMessage(){
        logger.info("{} occurred. current scanRate is {}ms and sending last stored message", MelsecConstants.DISCONNECT_WARNING_MSG, scanRate);
    }
    private void cycletoNextport(){
        if(isRedundancy()){
            logger.info("{}, now trying to move alternate communication port", MelsecConstants.DISCONNECT_WARNING_MSG);
            currentPortIndex++;
            if(currentPortIndex==redundancyAddressSize){ currentPortIndex = 0;}
            initClient(Integer.parseInt(redundancyAddress.get(currentPortIndex)));
            logger.info("Port change Completed now using '{}' port number", redundancyAddress.get(currentPortIndex));
        }
        else { throw new StageException (Errors.ERROR_001); }
    }
    @NotNull
    private List<Object> sendStringData(@NotNull AddressListStore item, BatchMaker batchMaker, long nextSourceOffset) throws StageException{
        String address = item.getAddress().toUpperCase();
        String prefix = getPrefix(address);
        int addressInt = getNumericAddress(address, prefix);
        int points = item.getPoints();
        CompletableFuture<Void> resultFuture = client.batchRead(address, points).thenAccept(r -> {
            StringBuilder result= new StringBuilder();
            for (int i = 0; i < points; i ++) {
                char hiChar, lowChar;
                if (isAscii) {
                    hiChar = (char) r.readByte();
                    lowChar = (char) r.readByte();
                } else {
                    lowChar = (char) r.readByte();
                    hiChar = (char) r.readByte();
                }
                result.append(hiChar).append(lowChar);
            }
            String resultAddress = prefix + setRadix(prefix, addressInt);
            if (transferMode==0) {
                if (isDuplicateValue(address, result.toString())) { sendRecord(resultAddress, result, batchMaker, nextSourceOffset); }
            }
            else {
                storeLastValue(address, result.toString());
                sendRecord(resultAddress, result.toString(), batchMaker, nextSourceOffset);
            }
            r.release();
        }).exceptionally(ex-> {
            printDisconnectMessage();
            String result = lastStringMap.get(address);
            if(transferMode==2 && result != null) { sendRecord(address, result, batchMaker, nextSourceOffset); }
            else { printErrorMessage(ex); }
            return null;
        });
        return setReturnValue(nextSourceOffset + points, resultFuture);
    }
    @NotNull
    private List<Object> sendRealNumberData(@NotNull AddressListStore item, int dataSize, BatchMaker batchMaker, long nextSourceOffset) throws StageException{
        String address = item.getAddress().toUpperCase();
        String prefix = getPrefix(address);
        int points = item.getPoints()*dataSize;
        int addressInt = getNumericAddress(address, prefix);
        CompletableFuture<Void> resultFuture = client.batchRead(address, points).thenAccept(r -> {
            int addressCount = addressInt;
            for (int i = 0; i < points; i += dataSize) {
                byte [][] readByte = new byte[dataSize][2];
                ByteBuf buffer = Unpooled.buffer(dataSize);
                for (int idx = 0; idx < dataSize; idx++) {
                    if(isAscii) {
                        readByte[idx][1] = r.readByte();
                        readByte[idx][0] = r.readByte();
                    }
                    else{
                        readByte[idx][0] = r.readByte();
                        readByte[idx][1] = r.readByte();
                    }
                }
                for (int j = dataSize - 1; j >= 0; j--) { buffer.writeBytes(readByte[j]); }
                double result;
                if(dataSize == 4){ result = buffer.readDouble(); }
                else { result = buffer.readFloat(); }
                String eachAddress = prefix + setRadix(prefix, addressCount);
                if (transferMode==0) {
                    if (isDuplicateValue(eachAddress, result)) { sendRecord(eachAddress, result, batchMaker, nextSourceOffset); }
                }
                else {
                    storeLastValue(address, result);
                    sendRecord(eachAddress, result, batchMaker, nextSourceOffset);
                }
                addressCount += dataSize;
            }
            r.release();
        }).exceptionally(ex-> {
            printDisconnectMessage();
            if(transferMode ==2) {
                printDisconnectMessage();
                int addressCount = addressInt;
                for (int i = 0; i < points; i += dataSize) {
                    String eachAddress = prefix + setRadix(prefix, addressCount);
                    Double result = lastNumberMap.get(eachAddress);
                    if (result != null) {
                        sendRecord(eachAddress, result, batchMaker, nextSourceOffset);
                    }
                    addressCount += dataSize;
                }
            }
            else { printErrorMessage(ex); }
            return null;
        });
        return setReturnValue(nextSourceOffset + points, resultFuture);
    }
    @NotNull
    private List<Object> sendWordsData(@NotNull AddressListStore item, int dataSize, BatchMaker batchMaker, long nextSourceOffset) throws StageException{
        String address = item.getAddress().toUpperCase();
        String prefix = getPrefix(address);
        int points = item.getPoints() * dataSize;
        int addressInt = getNumericAddress(address, prefix);
        CompletableFuture<Void> resultFuture = client.batchRead(address, points).thenAccept(r -> {
            int addressCount = addressInt;
            int bitShfitNum = (dataSize*16)-16;
            for (int i = 0; i < points; i += dataSize) {
                int[] intArray = getResultArrayNumber(r, dataSize);
                double result=0;
                for (int value : intArray) {
                    result += ((long) value << bitShfitNum);
                    bitShfitNum -= 16;
                }
                result = result*item.getScale();
                String eachAddress = prefix + setRadix(prefix, addressCount);
                if (transferMode==0) {
                    if (isDuplicateValue(eachAddress, result)) { sendRecord(eachAddress, result, batchMaker, nextSourceOffset); }
                }
                else {
                    storeLastValue(eachAddress, result);
                    sendRecord(eachAddress, result, batchMaker, nextSourceOffset);
                }
                addressCount += dataSize;
            }
            r.release();
        }).exceptionally(ex-> {
            printDisconnectMessage();
            if(transferMode==2) {
                printDisconnectMessage();
                int addressCount = addressInt;
                for (int i = 0; i < points; i += dataSize) {
                    String eachAddress = prefix + setRadix(prefix, addressCount);
                    Double result = lastNumberMap.get(eachAddress);
                    if(result!=null) { sendRecord(eachAddress, result, batchMaker, nextSourceOffset); }
                    addressCount += dataSize;
                }
            }
            else { printErrorMessage(ex); }
            return null;
        });
        return setReturnValue(nextSourceOffset + points, resultFuture);
    }

    @NotNull
    private int[] getResultArrayNumber(ByteBuf response, int arrayLength) {
        int[] result = new int[arrayLength];
        for (int i = arrayLength - 1; i > 0; i--) {
            if (isAscii) { result[i] = response.readUnsignedShortLE(); }
            else { result[i] = response.readUnsignedShort(); }
        }
        if(isAscii) {result[0] = response.readShortLE();}
        else { result[0] = response.readShort(); }
        return result;
    }
    private int getNumericAddress(String address, String prefix) throws StageException {
        int addressInt;
        try { addressInt = Integer.parseInt(address.split(prefix)[1], getRadix(prefix)); }
        catch (Exception e){ throw new StageException (Errors.ERROR_030, e.getMessage()); }
        return addressInt;
    }

    @NotNull
    private String getPrefix(@NotNull String address) {
        String result;
        String item = address.toUpperCase();
        if(item.startsWith(MelsecConstants.TN_REG_PREFIX)|| item.startsWith(MelsecConstants.TS_REG_PREFIX)||address.startsWith(MelsecConstants.TC_REG_PREFIX)||
                item.startsWith(MelsecConstants.SN_REG_PREFIX)|| item.startsWith(MelsecConstants.SS_REG_PREFIX)||item.startsWith(MelsecConstants.SC_REG_PREFIX)||
                        item.startsWith(MelsecConstants.CN_REG_PREFIX)|| item.startsWith(MelsecConstants.CS_REG_PREFIX)||item.startsWith(MelsecConstants.CC_REG_PREFIX)){
            result = address.substring(0, 2);
        }
        else {result = address.substring(0,1);}
        return result;
    }

    @NotNull
    private String setRadix(String prefix, int addressCount) {
        StringBuilder result;
        if(getRadix(prefix)==16){ result = new StringBuilder(Integer.toHexString(addressCount)); }
        else { result = new StringBuilder(String.valueOf(addressCount)); }
        while (result.length()<6){
            result.insert(0, "0");
        }
        return result.toString();
    }
    private int getRadix(@NotNull String prefix) {
        int result;
        String item = prefix.toUpperCase();
        if (item.startsWith(MelsecConstants.X_REG_PREFIX) || item.startsWith(MelsecConstants.Y_REG_PREFIX) ||
                item.startsWith(MelsecConstants.B_REG_PREFIX) || item.startsWith(MelsecConstants.W_REG_PREFIX)) {
            result = 16;
        }
        else { result = 10; }
        return result;
    }
    private void storeLastValue(String addressString, Object value){ lastBoolIntMap.put(addressString, value); }
    private void storeLastValue(String addressString, String value){ lastStringMap.put(addressString, value); }
    private void storeLastValue(String addressString, double value){ lastNumberMap.put(addressString, value); }
    @NotNull
    private List<Object> setReturnValue(long offset, CompletableFuture<Void> resultFuture){
        List<Object> resultList = new ArrayList<>();
        resultList.add(offset);
        resultList.add(resultFuture);
        return resultList;
    }
    private boolean isDuplicateValue(String addressString, Object value){
        boolean returnResult;
        if(lastBoolIntMap.containsKey(addressString)){
            if(lastBoolIntMap.get(addressString)==value){ returnResult = true;}
            else {
                lastBoolIntMap.put(addressString, value);
                returnResult=false;
            }
        }
        else {
            lastBoolIntMap.put(addressString, value);
            returnResult = false;
        }
        return !returnResult;
    }
    private boolean isDuplicateValue(String addressString, String value){
        boolean returnResult;
        if(lastStringMap.containsKey(addressString)){
            if(lastStringMap.get(addressString).equals(value)){ returnResult = true;}
            else {
                lastStringMap.put(addressString, value);
                returnResult=false;
            }
        }
        else {
            lastStringMap.put(addressString, value);
            returnResult = false;
        }
        return !returnResult;
    }
    private boolean isDuplicateValue(String addressString, double value){
        boolean returnResult;
        if(lastNumberMap.containsKey(addressString)){
            if(lastNumberMap.get(addressString)==value){ returnResult = true;}
            else {
                lastNumberMap.put(addressString, value);
                returnResult=false;
            }
        }
        else {
            lastNumberMap.put(addressString, value);
            returnResult = false;
        }
        return !returnResult;
    }

    private <T> void sendRecord(String address, T value, @NotNull BatchMaker batchMaker, long nextSourceOffset){
        Record record = getContext().createRecord(String.valueOf(nextSourceOffset));
        Map <String, Field> fields = new HashMap<>();
        Field.Type type = getType(value);
        fields.put("address", Field.create(address));
        fields.put("value", Field.create(type, value));
        fields.put("timestamp", Field.create(System.currentTimeMillis()));
        record.set(Field.create(fields));
        batchMaker.addRecord(record);
    }
    @NotNull
    private List<CompletableFuture<Void>> sendReplyMessage(){
        List <CompletableFuture<Void>> result = new ArrayList<>();
        for (String key : replyMessageMap.keySet()) {
            ByteBuf writeData = Unpooled.buffer(1);
            try {
                writeData.writeShort(Integer.parseInt(replyMessageMap.get(key)));
                CompletableFuture<Void> eachResult = client.batchWrite(key, 1, writeData);
                result.add(eachResult);
            } catch (NumberFormatException ne) {
                writeData.writeBoolean(Boolean.parseBoolean(replyMessageMap.get(key)));
                CompletableFuture<Void> eachResult = client.batchWrite(key, 1, writeData);
                result.add(eachResult);
            }
        }
        return result;
    }
    private <T> Field.Type getType(T value) {
        Field.Type result;
        if(value instanceof Integer){ result = Field.Type.INTEGER; }
        else if (value instanceof Boolean){ result = Field.Type.BOOLEAN; }
        else if (value instanceof Long) {result = Field.Type.LONG; }
        else if (value instanceof Double){ result = Field.Type.DOUBLE; }
        else if (value instanceof Float){ result = Field.Type.DOUBLE; }
        else { result = Field.Type.STRING; }
        return result;
    }

    private boolean checkConnectivity() throws StageException{
        Socket socket = null;
        try {
            socket = new Socket(getIPAddress(), getPort());
            socket.setSoTimeout(scanRate);
        }
        catch (IOException e) { return false; }
        finally {
            try {
                assert socket != null;
                socket.close();
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }
    private void printErrorMessage(Throwable ex){
        ex.printStackTrace();
        //logger.warn("Exception Message is :::"+ Arrays.toString(ex.getStackTrace()));
        exceptionOccurred = true;
    }
}