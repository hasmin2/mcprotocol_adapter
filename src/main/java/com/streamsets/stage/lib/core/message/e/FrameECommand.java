package com.streamsets.stage.lib.core.message.e;


import com.streamsets.stage.lib.core.message.Principal;

/**
 * @author liumin
 */
public interface FrameECommand extends FrameEMessage {

    /**
     * 获取主体
     *
     * @return 主体
     */
    Principal getPrincipal();
}
