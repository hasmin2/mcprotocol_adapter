/**
 * Copyright 2015 StreamSets Inc.
 * <p>
 * Licensed under the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.streamsets.stage.destination;

import com.streamsets.pipeline.api.*;
import com.streamsets.pipeline.lib.el.RecordEL;
import com.streamsets.stage.lib.MelsecConstants;
import com.streamsets.stage.lib.menus.*;

import java.util.List;

@StageDef(
        version = 1,
        label = MelsecConstants.MELSEC_TARGET_LABEL,
        description = MelsecConstants.MELSEC_TARGET_DESC,
        icon = MelsecConstants.MELSEC_ICON_NAME,
        recordsByRef = true,
        onlineHelpRefUrl = ""
)
@ConfigGroups(value = Groups.class)
@GenerateResourceBundle
public class McprotocolDTarget extends McprotocolTarget {
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            defaultValue = MelsecConstants.NETTYPE_ETH_OPTION,
            label = MelsecConstants.COMMTYPE_LABEL,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP,
            description = MelsecConstants.COMMTYPE_DESC
    )
    @ValueChooserModel(NetworkTypeChooserValues.class)
    public NetworkType commType;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.STRING,
            defaultValue = MelsecConstants.IP_VALUE,
            label = MelsecConstants.IP_ADDR_LABEL,
            dependsOn = "commType",
            elDefs = {RecordEL.class},
            evaluation = ConfigDef.Evaluation.EXPLICIT,
            triggeredByValue = MelsecConstants.NETTYPE_ETH_OPTION,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP,
            description = MelsecConstants.IP_ADDR_DESC
    )
    public String ipAddress;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.STRING,
            defaultValue = "address",
            label = MelsecConstants.ADDRESS_TO_WRITE_LABEL,
            displayPosition = 10,
            group = MelsecConstants.ADDRESS_GROUP,
            description = MelsecConstants.ADDRESS_TO_WRITE_DESC
    )
    public String inputAddressName;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.STRING,
            defaultValue = "value",
            label = MelsecConstants.VALUE_TO_WRITE_LABEL,
            displayPosition = 10,
            group = MelsecConstants.ADDRESS_GROUP,
            description = MelsecConstants.VALUE_TO_WRITE_DESC
    )
    public String inputTagName;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.STRING,
            defaultValue = "dataType",
            label = MelsecConstants.INPUT_DATA_TYPE_LABEL,
            displayPosition = 10,
            group = MelsecConstants.ADDRESS_GROUP,
            description = MelsecConstants.INPUT_DATA_TYPE_DESC
    )
    public String inputDataTypeName;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = "0",
            label = MelsecConstants.PC_NETWORK_NO_LABEL,
            description = MelsecConstants.PC_NETWORK_NO_DESC,
            min=0,
            max=255,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP
    )
    public int pcNetworkNumber;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = "255",
            label = MelsecConstants.PC_STATION_NO_LABEL,
            description = MelsecConstants.PC_STATION_NO_DESC,
            min=0,
            max=255,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP
    )
    public int pcStationNumber;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            defaultValue = "CPULOCAL",
            label = MelsecConstants.MELSEC_CPULOCATION_LABEL,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP,
            description = MelsecConstants.MELSEC_CPU_LOCATION_DESC
    )
    @ValueChooserModel(MelsecCPULocationChooserValues.class)
    public MelsecCPULocation cpuLocation;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = "0",
            label = MelsecConstants.MELSEC_STATION_NO_LABEL,
            description = MelsecConstants.MELSEC_STATION_NO_DESC,
            min=0,
            max=31,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP
    )
    public int melsecStationNumber;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = MelsecConstants.PORT_VALUE,
            label = MelsecConstants.PORT_LABEL,
            dependsOn = "commType",
            triggeredByValue = MelsecConstants.NETTYPE_ETH_OPTION,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP,
            description = MelsecConstants.PORT_DESC
    )
    public int port;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.STRING,
            defaultValue = MelsecConstants.COMPORT_VALUE,
            label = MelsecConstants.COMPORT_LABEL,
            dependsOn = "commType",
            triggeredByValue = MelsecConstants.NETTYPE_SER_OPTION,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP,
            description = MelsecConstants.COMPORT_DESC
    )
    public String comPort;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            defaultValue = "",
            label = MelsecConstants.NETWORK_CONNECTION_TYPE_LABEL,
            description = MelsecConstants.NETWORK_CONNECTION_TYPE_DESC,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP
    )
    @ValueChooserModel(NetworkConnectionTypeChooserValues.class)
    public NetworkConnectionType networkConnectionType;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            label = MelsecConstants.CONTENT_TYPE_LABEL,
            description = MelsecConstants.CONTENT_TYPE_DESC,
            displayPosition = 30,
            group = MelsecConstants.COMM_GROUP
    )
    @ValueChooserModel(InputDataFormatChooserValues.class)
    public InputDataFormat inputDataFormat;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            label = MelsecConstants.PROTOCOL_TYPE_LABEL,
            description = MelsecConstants.PROTOCOL_TYPE_DESC,
            displayPosition = 210,
            group = MelsecConstants.COMM_GROUP
    )
    @ValueChooserModel(ProtocolTypeChooserValues.class)
    public ProtocolType protocolType;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.BOOLEAN,
            defaultValue = "FALSE",
            label = MelsecConstants.REDUNDANCY_ENABLE_LABEL,
            description = MelsecConstants.REDUNDANCY_ENABLE_DESC,
            displayPosition = 1,
            group = MelsecConstants.REDUNDANCY_GROUP
    )
    public boolean redundancyEnabled;
    @ConfigDef(
            required = false,
            type = ConfigDef.Type.LIST,
            label = MelsecConstants.REDUNDANCY_ADDRLIST_LABEL,
            description = MelsecConstants.REDUNDANCY_ADDRLIST_DESC,
            displayPosition = 10,
            group = MelsecConstants.REDUNDANCY_GROUP,
            dependsOn = "redundancyEnabled",
            triggeredByValue = "true"
    )
    @ListBeanModel
    public List<String> redundancyAddressList;

    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = "6000",
            label = MelsecConstants.ADDR_CYCLE_TIME_LABEL,
            description = MelsecConstants.ADDR_CYCLE_TIME_DESC,
            min=1000,
            max=60000,
            displayPosition = 20,
            group = MelsecConstants.COMM_GROUP
    )
    public int addr_Cycle_Timeout;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRedundancy() { return redundancyEnabled; }
    @Override
    public int getAddr_Cycle_Timeout() { return addr_Cycle_Timeout; }
    @Override
    public List<String> getRedundancyAddressList() {return redundancyAddressList; }
    @Override
    public String getInputDataTypeName(){ return inputDataTypeName;}
    @Override
    public String getInputAddressName() {return inputAddressName;}
    @Override
    public String getInputTagName() {return inputTagName;}
    @Override
    public String getInputDataFormat() {
        String result = null;
        switch(inputDataFormat.name()){
            case "MULTIPLEJSON":
                result = inputDataFormat.getLabel();
            case "JSONARRAY":
                result = inputDataFormat.getLabel();
        }
        return result;
    }
    @Override
    public String getNetworkType() {return commType.getLabel();}
    @Override
    public String getIPAddress() {return ipAddress;}
    @Override
    public int getPort() {return port;}
    @Override
    public String getComPort() {return comPort;}
    @Override
    public String getNetworkConnectionType() {return networkConnectionType.getLabel();}
    @Override
    public int getPcNetworkNumber() {return pcNetworkNumber;}
    @Override
    public int getPcStationNumber() {
        int result;
        if(pcNetworkNumber==0) {result = 255;}
        else { result = pcStationNumber; }
        return result ;
    }
    @Override
    public int getMelsecCPULocation() {
        int result;
        switch (cpuLocation.name()) {
            case "CPULOCAL":
                result = 0x03FF;
                break;
            case "CPUNO1":
                result = 0x03E0;
                break;
            case "CPUNO2":
                result = 0x03E1;
                break;
            case "CPUNO3":
                result = 0x03E2;
                break;
            case "CPUNO4":
                result = 0x03E3;
                break;
            default:
                result = 0;
        }
        return result;
    }
    @Override
    public int getMelsecStationNumber(){ return melsecStationNumber;}
    @Override
    public String getProtocolType(){ return protocolType.getLabel();}


}
