/**
 * Copyright 2015 StreamSets Inc.
 * <p>
 * Licensed under the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.streamsets.stage.destination;

import com.streamsets.pipeline.api.Field;
import com.streamsets.pipeline.api.Record;
import com.streamsets.pipeline.sdk.RecordCreator;
import com.streamsets.pipeline.sdk.TargetRunner;
import com.streamsets.stage.lib.menus.*;
import org.junit.Test;

import java.util.*;

import static java.lang.Thread.sleep;

public class TestMcprotocolTarget {
    @Test
    public void testWriteSingleRecord() throws Exception {
        List<String> alterAddress = new ArrayList<>();
        alterAddress.add("5001");
        alterAddress.add("5000");

        TargetRunner runner = new TargetRunner.Builder(McprotocolDTarget.class)
                .addConfiguration("melsecStationNumber", 0)
                .addConfiguration("protocolType", ProtocolType.BINARY)
                .addConfiguration("cpuLocation", MelsecCPULocation.CPULOCAL)
                .addConfiguration("pcStationNumber", 255)
                .addConfiguration("pcNetworkNumber", 0)
                .addConfiguration("networkConnectionType", NetworkConnectionType.TCPIPTYPE)
                //.addConfiguration("ipAddress", "${record:value('/ipAddress')}")
                .addConfiguration("ipAddress", "10.7.10.181")
                .addConfiguration("port",5100)
                .addConfiguration("inputTagName", "value")
                .addConfiguration("inputDataTypeName", "dataType")
                .addConfiguration("inputAddressName", "address")
                .addConfiguration("inputDataFormat", InputDataFormat.MULTIPLEJSON)
                .addConfiguration("commType", NetworkType.ETHERNET)
                .addConfiguration("redundancyEnabled", false)
                .addConfiguration("addr_Cycle_Timeout", 3000)
               // .addConfiguration("redundancyAddressList", alterAddress)
                .build();

        runner.runInit();
        int temp = 22;
        Record record = RecordCreator.create();
        Map<String, Field> fields = new HashMap<>();
        fields.put("ipAddress", Field.create("10.7.10.181"));
        fields.put("address", Field.create("D6909"));
        fields.put("value", Field.create(temp));
        fields.put("dataType", Field.create("SIGNED_INTEGER"));
        record.set(Field.create(fields));
        runner.runWrite(Arrays.asList(record));
        sleep(3000);
        temp = 24;
        record = RecordCreator.create();
        fields = new HashMap<>();
        fields.put("ipAddress", Field.create("10.7.10.181"));
        fields.put("address", Field.create("D6909"));
        fields.put("value", Field.create(temp));
        fields.put("dataType", Field.create("SIGNED_INTEGER"));
        record.set(Field.create(fields));
        runner.runWrite(Arrays.asList(record));
        // Here check the data destination. E.g. a mock.

        runner.runDestroy();
    }
}
