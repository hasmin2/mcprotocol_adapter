package com.streamsets.stage.lib.menus;

import com.streamsets.pipeline.api.base.BaseEnumChooserValues;

public class TransferModeSelectChooserValues extends BaseEnumChooserValues<TransferModeSelect> {
    public TransferModeSelectChooserValues() {super(TransferModeSelect.class);}
}
