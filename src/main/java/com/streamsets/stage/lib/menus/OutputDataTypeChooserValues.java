package com.streamsets.stage.lib.menus;

import com.streamsets.pipeline.api.base.BaseEnumChooserValues;

public class OutputDataTypeChooserValues extends BaseEnumChooserValues<OutputDataType> {
    public OutputDataTypeChooserValues() {
        super(OutputDataType.class);
    }
}
