package com.streamsets.stage.lib.util;


import com.streamsets.stage.lib.MelsecConstants;
import com.streamsets.stage.lib.menus.CustomDataListMenu;
import com.streamsets.stage.lib.menus.OutputDataType;

import java.util.ArrayList;
import java.util.List;

public class AddressListGenerator {
    //////////////////////////추가해야함.
        private List<AddressListStore> addressResult  = new ArrayList<>();
    //from UI
    public AddressListGenerator(List<CustomDataListMenu> customDataListMenuList) {
        for (CustomDataListMenu item : customDataListMenuList) {
            AddressListStore addressListStore = new AddressListStore();
            addressListStore.setAddress(item.startAddress);
            if(item.dataType==null) { addressListStore.setDataType(OutputDataType.BOOLEAN); }
            else { addressListStore.setDataType(item.dataType); }
            addressListStore.setPoints(item.points);
            addressListStore.setScale(item.scale/MelsecConstants.SCALE_DIVIDE_VALUE);
            addressResult.add(addressListStore);
        }
    }
    //////////////////////////////////////////////////////////////

    public List<AddressListStore> getAddressList(){ return addressResult;}

}
