# MC Protocol_origin

This code is for "reading" from Melsec TCP (Udp/Serial will be implement).
Supports each data output's data format, little/big endian reading.
 
This adapter is based on **Streamsets 3.11**



## Getting Started

This source code is based on Streamsets 3.11, Therefore you need to know basis of streamsets and custom origin's concept.

you can find it from https://github.com/streamsets/tutorials/blob/master/tutorial-origin/readme.md



### Prerequisites

I used latest version of IntelliJ and oracle Jdk 8-202. OpenJdk compatibility has not been tested. But should be no problem.

```
Intellij, and JDK 8 or later
```

### Installing

following command should be used to build "mcprotocol-adapter-{$version}.tar.gz".

The "tar.gz" file should be copied into "${Streamsets_root}/user-lib" and decompressed.

```
mvn package -DskipTests 
....
cp [streamsets_root]/etc/user/lib/mcprotocol-adapter-{$version}.tar.gz
tar -xvf mcprotocol-adapter-{version}.tar.gz
```



Also special permision should be required to get TCP connection and Command console.

open the  "${Streamsets_root}/etc/sdc-security.properties" 

and put line as below;

```
grant codebase "file://${sdc.dist.dir}/user-libs/mcprotocol-adapter/-" {
  permission java.security.AllPermission;
};
```

You Should restart the Streamsets. if you paste files while streamsets is running.

### Running in Streamsets / USAGE
Once you open the streamsets pipeline, you can see below image like,

<hr>

![Melsec Adapter Orogin_Communication](./image/Melsec_Origin_Comm.png)
 1. Communication Type
    1) Ethernet(TCPIP/UDP) : Modbus uses ethernet connection it use 'IP' and 'port' to connect.
    2) Serial(RS232/485)(not Implemented) : Modbus Uses RS232/RS485 Connection, it uses serial string such '/dev/ttyUSB0', implemented only, not tested so far.
 
2. IP Address (Ethernet option only) : input IP address.

    Subscribe/polling : if checked, skip value compare last values. reduce data amount with duplicated data.

3. Boolean output type
 
    1) Converted to Integer value (1/0) output : True/false value will changed into 1/0 integer.
 
    2) Original (true/false) output
 
4. scan rate (Millises) : Select a Communication rate, note that set to '0' for send as soon as can
 
5. Network(PC) : Network station number from connecting PC side leave it as default.
 
6. Station (PC) : Station ID from PC side. option will be disabled if "Network (PC) option is set to 0.
 
    ![Melsec_Adapter_PC_NETNO](./image/net_pcno.png)
 
    network and station binary code from melsec manual 
 
7. CPU Location : Select a CPU station ID as per below image.
 
    ![Melsec Adapter CpuLocation](./image/CpuLocation.png)
 
    Cpu location binary code from melsec manual
    
8. Station (PLC) : specify station number for PLC side.
 
9. Timeout (Millisecs) : specify timeout for waiting plc connection reply. 

10. Port/Port String

    1) Port (Ethernet option only) : select Mc protocol TCP port. default is 5000.
    
    2) Port String (Serial option only): input serial port path for connecting.
 
 11. Network Connection Type 
    
    1) TCP/IP : using TCP/IP for connection, default value is 5000.
    
    2) UDP : using UDP for connection (not implemented).
 
 12. Network Comm. protocol
    
    1) Binary : Use binary for communication ex) DEC 16 -> {0x10}
    
    2) ASCII : Use ascii code for communication ex) DEC16 0-> {0x30, 0X36}
    
<hr> 
 
![Melsec Origin_Address](./image/Melsec_Origin_Address.png)

1. read from csv file 
    
    1) Read KEPWARE from home directory for address configure. not from UI. Note that the csv format is export from KepServer.
       1. not implemented yet!
       
    2) Input my list using UI web interface directly.
        1. Start Address(Hex/DEC) : Input proper address string ex)X0, X000, D0, M1
        
        2. Points to read : Input proper numbers to be read, Flexible for data type. ex) Read points 10 with Qword  will read 40 word read automatically.
        
        3. DataType : select proper data Type for read
        
        4. Seale : Define proper scale with numbers, note that 10000 will be initial value for 1:1 read. thus up to 1/10000 can be presented.
    
2. reply message(Optional) : Input reply message for each read. Should proper **[address:value]**, ex) X000:true -> write every record X000 with true. 

<hr>

1. Redundancy enable? : Enable/disables Redundancy port list. not available when 'Transfer Mode' option is 'Always'.

2. Address List : specify define address as a list. Each address list is cycling every 'Timeout(port cycle time)' period' user can read 'log' that the port is now changing. However, UI does not show any error messages except no message is coming. If no input any address, the client continue connect/disconnect every timeout period if port is not available.

<hr>

![Melsec Dest_Address](./image/Melsec_Dest_Address.png)
1. Tag name for address extracting : Specify name for 'ADDRESS' tag that include address string. default is 'address'

2. Tag name for value extracting : Specify name for 'VALUE' tag that include types of value. default is 'value'

3. Tag name for dataType extracting : Specify name for 'DATATYPE' tag that include dataType String default is 'dataType' 
    - note that the data Type should either **{BOOLEAN| WORD | DWORD | QWORD | UNSIGNED_INTEGER | SIGNED_INTEGER}**

<hr>
                  
![Melsec Dest_Comm](./image/Melsec_Dest_Comm.png)
1. Communication Type
 
    1) Ethernet(TCPIP/UDP) : Modbus uses ethernet connection it use 'IP' and 'port' to connect.

    2) Serial(RS232/485)(not Implemented) : Modbus Uses RS232/RS485 Connection, it uses serial string such '/dev/ttyUSB0', implemented only, not tested so far.
    
 2. IP Address (Ethernet option only) : input IP address.
 
 3. Subscribe/polling : if checked, skip value compare last values. reduce data amount with duplicated data.
 
 4. Network(PC) : Network station number from connecting PC side leave it as default.
 
 5. Station (PC) : Station ID from PC side. option will be disabled if "Network (PC) option is set to 0.
 
    ![Melsec_Adapter_PC_NETNO](./image/net_pcno.png)
 
    network and station binary code from melsec manual 
 6. CPU Location : Select a CPU station ID as per below image.
    ![Melsec Adapter CpuLocation](./image/CpuLocation.png)
 
    Cpu location binary code from melsec manual
    
 7. Station (PLC) : specify station number for PLC side.
 
 8. Timeout (Millisecs) : specify timeout for waiting plc connection reply. 

 9. Port/Port String
 
    1) Port (Ethernet option only) : select Mc protocol TCP port. default is **5000**.
 
    2) Port String (Serial option only): input serial port path for connecting.
 
 10. Network Connection Type 
 
    1) TCP/IP : using TCP/IP for connection, default value is **5000**.
 
    2) UDP : using UDP for connection (not implemented).
 
 11. Network Comm. protocol
 
     1) Binary : Use binary for communication ex) DEC 16 -> {0x10}
 
     2) ASCII : Use ascii code for communication ex) DEC16 0-> {0x30, 0X36}

<hr>

1. Redundancy enable? : Enable/disables Redundancy port list. not available when 'Transfer Mode' option is 'Always'.

2. Address List : specify define address as a list. Each address list is cycling every 'Timeout(port cycle time)' period' user can read 'log' that the port is now changing. However, UI does not show any error messages except no message is coming. If no input any address, the client continue connect/disconnect every timeout period if port is not available. 
 
 <hr>
 
## Acknowledgments

Refrenced below java code.
  
  __modraedlau__
  
  https://github.com/modraedlau/melsec
 
 Thank you very much for providing base code!!!

- Hat tip to anyone who's code was used
- Inspiration
- etc