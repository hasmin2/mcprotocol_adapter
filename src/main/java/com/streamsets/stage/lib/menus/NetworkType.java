package com.streamsets.stage.lib.menus;

import com.streamsets.pipeline.api.Label;
import com.streamsets.stage.lib.MelsecConstants;

public enum NetworkType implements Label {
    ETHERNET(MelsecConstants.OPTION_VALUE_ETHERNET_TYPE),
    SERIAL(MelsecConstants.OPTION_VALUE_SERIAL_TYPE);

    private final String label;

    NetworkType(String label) {
        this.label = label;
    }

    @Override
    public String getLabel() {
        return label;
    }
}