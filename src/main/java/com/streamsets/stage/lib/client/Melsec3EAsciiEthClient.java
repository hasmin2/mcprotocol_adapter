package com.streamsets.stage.lib.client;


import com.streamsets.stage.lib.core.MelsecClientOptions;
import com.streamsets.stage.lib.core.message.Function;
import com.streamsets.stage.lib.core.message.e.Frame3EAsciiCommand;
import com.streamsets.stage.lib.core.message.e.FrameEResponse;
import com.streamsets.stage.lib.melsec.codec.ClientFrame3EAsciiMessageDecoder;
import com.streamsets.stage.lib.melsec.codec.ClientFrameEMessageEncoder;
import com.streamsets.stage.lib.melsec.codec.Frame3EAsciiByteDecoder;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelPipeline;

import java.util.concurrent.CompletableFuture;

/**
 * @author liumin
 */
public class Melsec3EAsciiEthClient extends AbstractEthClient {
    Melsec3EAsciiEthClient(MelsecClientConfig config) {
        super(config);
    }

    @Override
    protected void initChannel(ChannelPipeline pipeline) {
        pipeline.addLast(ClientFrameEMessageEncoder.INSTANCE);
        pipeline.addLast(new Frame3EAsciiByteDecoder());
        pipeline.addLast(new ClientFrame3EAsciiMessageDecoder());
    }

    @Override
    public CompletableFuture<ByteBuf> batchRead(String address, int points) {
        CompletableFuture<ByteBuf> future = new CompletableFuture<>();
        CompletableFuture<FrameEResponse> response = sendRequest(new Frame3EAsciiCommand(
                Function.BATCH_READ,
                address,
                points,
                new MelsecClientOptions(config.getNetworkNo(),
                        config.getPcNo(),
                        config.getRequestDestinationModuleIoNo(),
                        config.getRequestDestinationModuleStationNo())));
        response.whenComplete((r, ex) -> {
            if (r != null) {
                future.complete(r.getData());
            } else {
                future.completeExceptionally(ex);
            }
        });
        return future;
    }

    @Override
    public CompletableFuture<Void> batchWrite(String address, int points, ByteBuf data) {
        CompletableFuture<Void> future = new CompletableFuture<>();
        CompletableFuture<FrameEResponse> response = sendRequest(new Frame3EAsciiCommand(
                Function.BATCH_WRITE,
                address,
                points,
                data,
                new MelsecClientOptions(config.getNetworkNo(),
                        config.getPcNo(),
                        config.getRequestDestinationModuleIoNo(),
                        config.getRequestDestinationModuleStationNo())));
        response.whenComplete((r, ex) -> {
            if (r != null) {
                future.complete(null);
            } else {
                future.completeExceptionally(ex);
            }
        });
        return future;
    }
}
