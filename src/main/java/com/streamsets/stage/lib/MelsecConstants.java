package com.streamsets.stage.lib;

public final class MelsecConstants {


    /////////////////////
    //Default Value and presets
    /////////////////////
    public static final int KEPWARE_CSV_COLUMN = 17;
    public static final String SLEEP_TIME = "0";
    public static final int READ_POINT_LIMIT = 240;
    public static final double SCALE_FACTOR_BASE = 10000.0;
    public static final String IP_PATTERN = "(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])";
    public static final String DISCONNECT_WARNING_MSG= "Message not received within timeout, check network connectivity or increase timeout";
    public static final String DISCONNECT_WRITE_WARNING_MSG= "Message cannot send within scan rate, check network connectivity";
    public static final String MELSEC_ICON_NAME = "mitsubishi.png";
    public static final String [] KEPWARE_DATA_TYPE_LIST = {"Default","String","Boolean","Char","Byte","Short","Word","Long","DWord","Float","Double","BCD","LBCD","Date","LLong","QWord","String Array","Boolean Array","Char Array","Byte Array","Short Array","Word Array","Long Array","DWord Array","Float Array","Double Array","BCD Array","LBCD Array","Date Array", "LLong Array", "QWord Array"};
    public static final int SCALE_DIVIDE_VALUE = 10000;
    public static final String FILEPATH_VALUE = "/home";
    public static final String SCANRATE_VALUE = "1000";
    public static final String PORT_VALUE = "5000";
    public static final String COMPORT_VALUE = "/dev/ttyS0";
    public static final String IP_VALUE = "localhost";

    ////////////////
    //model values BooleanOutput type
    ///////////////
    public static final String OPTION_VALUE_BOOLEAN_TYPE = "Original (True/false) output";
    public static final String OPTION_VALUE_INTEGER_TYPE = "Converted to Integer value (1/0) output";

    ////////////////
    //communication type option values
    ///////////////
    public static final String OPTION_VALUE_ETHERNET_TYPE = "Ethernet (TCPIP/UDP)";
    public static final String OPTION_VALUE_SERIAL_TYPE = "Serial (RS232/RS485)";
    //network type values
    public static final String OPTION_VALUE_TCPIP = "TCP/IP";
    public static final String OPTION_VALUE_UDP = "UDP";
    //Destination input data format values
    public static final String OPTION_VALUE_MULTIPLE_JSON_FORMAT = "Multiple JSON messages";
    public static final String OPTION_VALUE_JSON_ARRAY_FORMAT = "JSON array messages";
    //network type values
    public static final String OPTION_VALUE_ASCII = "ASCII";
    public static final String OPTION_VALUE_BINARY = "Binary";

    //Data TYPE option VALUES
    public static final String OPTION_VALUE_BOOLEAN = "Boolean(1Bit)";
    public static final String OPTION_VALUE_FLOAT = "Float (Single precision - 32Bit)";
    public static final String OPTION_VALUE_DOUBLE = "Double(Double precision - 64Bit)";
    public static final String OPTION_VALUE_LONG = "Long Integer(32Bit)";
    public static final String OPTION_VALUE_UNSIGNED_INTEGER = "Unsigned Integer(16Bit)";
    public static final String OPTION_VALUE_SIGNED_INTEGER = "Signed Integer(16Bit)";
    public static final String OPTION_VALUE_QWORD = "Quad Word(64Bit Signed)";
    public static final String OPTION_VALUE_STRING = "String (Various length, ASCII Read)";
    public static final String OPTION_VALUE_DWORD = "Double Word(32Bit Signed)";
    public static final String OPTION_VALUE_WORD = "Word (16Bit, ASCII Read)";
    public static final String OPTION_VALUE_ONLY_CHANGE = "Send on value change : Send value only changing. Minimize data transfer. 'scanRate' is able to dynamically increase until max boundary for proper communication";
    public static final String OPTION_VALUE_TRANSFER_ALWAYS = "Send always : Send data always even offline. Last data will be transferred while offline. Max data transfer usage, 'not affected by adjustScanRate'";
    public static final String OPTION_VALUE_WHILE_ONLINE_ONLY = "Send data while online : Send all data while online, data send stop while network is offline. relatively much data usage, same scanRate adjust function is applying";

    //CPU LOCATION OPTION VALUES
    public static final String OPTION_VALUE_CPU_LOCAL = "Local CPU";
    public static final String OPTION_VALUE_CPU_CONTROL = "Control CPU";
    public static final String OPTION_VALUE_CPU_NO1 = "CPU No.1";
    public static final String OPTION_VALUE_CPU_NO2 = "CPU No.2";
    public static final String OPTION_VALUE_CPU_NO3 = "CPU No.3";
    public static final String OPTION_VALUE_CPU_NO4 = "CPU No.4";
    public static final String OPTION_VALUE_CPU_STANDBY = "Standby CPU";
    public static final String OPTION_VALUE_CPU_SYSTEM_A = "System A CPU";
    public static final String OPTION_VALUE_CPU_SYSTEM_B = "System B CPU";

    //get address source selection
    public static final String OPTION_VALUE_LIST_FROM_UI = "Input my list using web page directly";
    public static final String OPTION_VALUE_KEPWARE_CSV = "Csv format (Kepserver V6, Industirial Gateway V8 or later)";

    /////////////////////
    //menugroups
    /////////////////////
    public static final String COMM_GROUP_LABEL = "Communication";
    public static final String ADDRESS_GROUP_LABEL = "Address";
    public static final String REDUNDANCY_GROUP_LABEL = "Redundancy";
    public static final String REDUNDANCY_GROUP = "REDUNDANCY";
    public static final String COMM_GROUP = "COMMUNICATION";
    public static final String ADDRESS_GROUP = "ADDRESS";
    public static final String NETTYPE_ETH_OPTION = "ETHERNET";
    public static final String NETTYPE_SER_OPTION = "SERIAL";
    public static final String KEPWARE_CSV_OPTION = "KEPWARECSV";
    public static final String CUSTOM_CSV_OPTION = "CUSTOMCSV";
    public static final String LIST_FROM_UI_OPTION = "LISTFROMUI";

    ///descriptions
    public static final String VALUE_TO_WRITE_DESC = "Put tag name that should parsing data value, default tag name is 'value'";
    public static final String NETWORK_CONNECTION_TYPE_DESC = "Select a network connection for MC protocol";
    public static final String COMPORT_DESC = "Serial port for MODBUS communication,  the port string should include extensions with case sensitive.\n ex) /dev/ttyUSB0";
    public static final String START_ADDRESS_DESC = "Specify input Start address, Make sure that format should fit HEX/DEC, Ex)D TAG in DEC, Y TAG in HEX";
    public static final String READ_POINT_DESC = "Specify read range in numbers, note that the max read points are 7904 as per Melsec manual. But DO NOT EXCEED 210 for ON/OFF, 1000 for WORD/INT/UINT, 500 for DWORD/FLOAT, 200 for QWORD/DOUBLE";
    public static final String INPUT_DATA_TYPE_DESC = "Specify tagname for input datatype, type should be either {BOOLEAN|WORD|DWORD|QWORD|STRING|UNSIGNED_INTEGER|SIGNED_INTEGER|LONG|DOUBLE|FLOAT}";
    public static final String TRANSFER_MODE_DESC = "Select proper data transfer mode for the system";
    public static final String MELSEC_ORIGIN_DESC = "Mitsubishi PLC melsec data Origin connector";
    public static final String MELSEC_TARGET_DESC = "Mitsubishi PLC melsec data Destination connector";
    public static final String IP_ADDR_DESC = "Set Mitsubishi PLC IP addrress (Default is 'localhost')";
    public static final String PORT_DESC = "Specify input port for Melsec can use of. Default value is 5000(UDP), 5100(TCP)";
    public static final String TIMEOUT_DESC = "Specify input TIMEOUT for Melsec can wait, default is 3 seconds(3000 msec)";
    public static final String MELSEC_TYPE_DESC = "Select a MODEL type for Mitsubishi melsec";
    public static final String MELSEC_CPU_LOCATION_DESC = "Select a CPU Location";
    public static final String MELSEC_DATA_TYPE_DESC = "Select proper data type";
    public static final String COMMTYPE_DESC = "Select a Communication Type";
    public static final String SCALE_DESC = "Input scale ratio by  1/10000. Default value is original value: '10000' ";
    public static final String DATA_MANAGE_SELECTION_DESC = "Select file format if you want read I/O Lists from local file instead of web page";
    public static final String FILEPATH_DESC = "Input file path for the file. the filename should include extensions with case sensitive. ex)'/home/myHome/exam.csv'. File path can be retrieved system administrator";
    public static final String BOOLEAN_OUTPUT_DESC = "If check, the boolean value is converted into 1/0 instead of True/False";
    public static final String SCANRATE_DESC = "Select a Communication rate, note that set to '0' for send as soon as can";
    public static final String PC_STATION_NO_DESC = "Station Number for PC Side, (only valid if PC network is 0)";
    public static final String PC_NETWORK_NO_DESC = "Network Number for PC Side";
    public static final String MELSEC_STATION_NO_DESC = "Network station number for PLC Side";
    public static final String PROTOCOL_TYPE_DESC = "Specify communication protocol, ASCII or BINARY";
    public static final String CONTENT_TYPE_DESC = "Specify message content format, JSON ARRAY or Multiple Json message";
    public static final String REPLY_MSG_DESC = "Define reply tag address and value, should 'tag:value' with colone(:) Bit tag should use true/false(1/0) data tag can use Short Integer(0~65535 or -32768-32767)";
    public static final String ADDRESS_TO_WRITE_DESC = "Put tag name that should parsing address name, default tag name is 'address'";
    public static final String REDUNDANCY_ADDRLIST_DESC = "Address list for redundancy, each address shall cycle for certain period that designated cycle time. Should input 1024~65535";
    public static final String ADDR_CYCLE_TIME_DESC = "Define a PLC timeout and cycle delay between each address for awaiting the response. default is 6000ms. should larger than scan rate. !!!Shorter timeout may cause freezing PLC devices!!!";
    public static final String REDUNDANCY_ENABLE_DESC = "Enables address cycle function for redundancy connection.";
    /////////////////LABELs
    public static final String ADDR_CYCLE_TIME_LABEL = "Timeout (Port cycle time) (ms)";
    public static final String REDUNDANCY_ENABLE_LABEL = "Redundancy enable?";
    public static final String INPUT_DATA_TYPE_LABEL = "Tag name for dataType extracting";
    public static final String VALUE_TO_WRITE_LABEL = "Tag name for value extracting";
    public static final String ADDRESS_TO_WRITE_LABEL = "Tag name for address extracting";
    public static final String MELSEC_TARGET_LABEL = "Mitsubishi Destination";
    public static final String REPLY_MSG_LABEL = "Reply message (Optional)";
    public static final String NETWORK_CONNECTION_TYPE_LABEL = "Network Connection Type";
    public static final String PROTOCOL_TYPE_LABEL = "Network Comm. protocol";
    public static final String REDUNDANCY_ADDRLIST_LABEL = "Address List";
    public static final String CONTENT_TYPE_LABEL = "Message Content type";
    public static final String CUSTOM_CSV_LABEL = "Custom CSV using AddressList format with 'RegisterType', 'startAddress and 'scale'";
    public static final String PC_STATION_NO_LABEL = "Station(PC)";
    public static final String PC_NETWORK_NO_LABEL = "Network(PC)";
    public static final String MELSEC_STATION_NO_LABEL = "Station (PLC)";
    public static final String SCALE_LABEL = "Scale";
    public static final String SCANRATE_LABEL = "Scan rate (Millisecs)";
    public static final String COMMTYPE_LABEL = "Communication Type";
    public static final String COMPORT_LABEL = "Serial port String";

    public static final String BOOLEAN_OUTPUT_LABEL = "Boolean output type";
    public static final String TRANSFER_MODE_LABEL = "Transfer Mode";
    public static final String MELSEC_ORIGIN_LABEL = "Mitsubishi Melsec Data Origin";
    public static final String START_ADDRESS_LABEL = "Start Address(HEX/DEC)";
    public static final String READ_POINT_LABEL = "Points to Read";
    public static final String FILEPATH_LABEL = "File path and name";
    public static final String DATA_MANAGE_SELECTION_LABEL = "Read from local stored file";
    public static final String DATA_TYPE_LABEL = "Data Type";
    public static final String IP_ADDR_LABEL = "IP Address";
    public static final String PORT_LABEL = "Port";
    public static final String MELSEC_TYPE_LABEL = "Model";
    public static final String MELSEC_CPULOCATION_LABEL = "CPU Location";
    public static final String MAXBLOCK_LABEL = "Maximum BlockSize (word unit)";


    //Data Type Option Label 데이터 타입별로 파싱해서 맵에 넣거나, 읽는 명령어가 달라짐 플래그로도 사용
    public static final String BOOLEAN_CASE = "BOOLEAN";
    public static final String WORD_CASE = "WORD";
    public static final String DWORD_CASE = "DWORD";
    public static final String QWORD_CASE = "QWORD";
    public static final String STRING_CASE = "STRING";
    public static final String UNSIGNED_INTEGER_CASE = "UNSIGNED_INTEGER";
    public static final String SIGNED_INTEGER_CASE = "SIGNED_INTEGER";
    public static final String LONG_CASE = "LONG";
    public static final String DOUBLE_CASE = "DOUBLE";
    public static final String FLOAT_CASE = "FLOAT";

    //REGISTER PREFIX
    public static final String X_REG_PREFIX = "X";
    public static final String Y_REG_PREFIX = "Y";
    public static final String M_REG_PREFIX = "M";
    public static final String L_REG_PREFIX = "L";
    public static final String F_REG_PREFIX = "F";
    public static final String V_REG_PREFIX = "V";
    public static final String B_REG_PREFIX = "B";
    public static final String D_REG_PREFIX = "D";
    public static final String W_REG_PREFIX = "W";
    public static final String TS_REG_PREFIX = "TS";
    public static final String TC_REG_PREFIX = "TC";
    public static final String TN_REG_PREFIX = "TN";
    public static final String SS_REG_PREFIX = "SS";
    public static final String SC_REG_PREFIX = "SC";
    public static final String SN_REG_PREFIX = "SN";
    public static final String CS_REG_PREFIX = "CS";
    public static final String CC_REG_PREFIX = "CC";
    public static final String CN_REG_PREFIX = "CN";


    //CPU TYPE OPTION
    public static final String Q_SERIES = "Q Series";
    public static final String A_SERIES = "A Series";
    public static final String FX3U = "FX3U";
    public static final String QNA_SERIES = "QnA Series";
    public static final String L_SERIES = "L Series";
    public static final String IQF_SERIES = "iQ-F Series";
    public static final String IQR_SERIES = "iQ-R Series";

    public static final String NUMBER_FORMAT_ERROR_MSG = "Address format Fault!";
    public static final String DATA_READ_FORMAT_ERROR_MSG = "Boolean type is not supported in this TAG";
}
