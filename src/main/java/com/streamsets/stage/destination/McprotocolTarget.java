/**
 * Copyright 2015 StreamSets Inc.
 * <p>
 * Licensed under the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.streamsets.stage.destination;

import com.streamsets.pipeline.api.Batch;
import com.streamsets.pipeline.api.Field;
import com.streamsets.pipeline.api.Record;
import com.streamsets.pipeline.api.StageException;
import com.streamsets.pipeline.api.base.BaseTarget;
import com.streamsets.pipeline.api.base.OnRecordErrorException;
import com.streamsets.pipeline.api.el.ELEval;
import com.streamsets.pipeline.api.el.ELVars;
import com.streamsets.pipeline.api.impl.Utils;
import com.streamsets.pipeline.lib.el.RecordEL;
import com.streamsets.stage.lib.Errors;
import com.streamsets.stage.lib.MelsecConstants;
import com.streamsets.stage.lib.client.MelsecClient;
import com.streamsets.stage.lib.client.MelsecClientConfig;
import com.streamsets.stage.origin.Groups;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;

/**
 * This target is an example and does not actually write to any destination.
 */
public abstract class McprotocolTarget extends BaseTarget {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * Gives access to the UI configuration of the stage provided by the {@link McprotocolDTarget} class.
     */
    public abstract String getInputTagName();
    public abstract boolean isRedundancy();

    public abstract int getAddr_Cycle_Timeout();

    public abstract List<String> getRedundancyAddressList();
    public abstract String getInputDataTypeName();
    public abstract String getInputAddressName();
    public abstract String getInputDataFormat();
    public abstract String getNetworkType();
    public abstract String getIPAddress();
    public abstract int getPort();
    public abstract String getComPort();
    public abstract String getNetworkConnectionType();
    public abstract int getPcNetworkNumber();
    public abstract int getPcStationNumber();
    public abstract int getMelsecCPULocation();
    public abstract int getMelsecStationNumber();
    public abstract String getProtocolType();
    private Map<String, Integer> addressIndexMap = new HashMap<>();
    private List<Integer> addressPortList = new ArrayList<>();
    private Map<String, MelsecClient> clientMap = new HashMap<>();
    private ELEval nameEval;
    private ELVars nameVars;
    private int port;
    private int cycleNextPort(String ipAddress){
        int result;
        if(!addressPortList.isEmpty()) {
            int curIndex = addressIndexMap.getOrDefault(ipAddress, 0);
            curIndex++;
            if(curIndex >= addressPortList.size()){ curIndex=0;}
            result = addressPortList.get(curIndex);
            addressIndexMap.put(ipAddress, curIndex);
            logger.info("Port change Completed now using '{}' port number", result);
        }
        else{ throw new StageException (Errors.ERROR_056); }
        return result;
    }

    @Override
    protected List<ConfigIssue> init() {
        List<ConfigIssue> issues = super.init();
        nameEval = getContext().createELEval("ipAddress");
        nameVars = getContext().createELVars();
        port = getPort();
        if(!getIPAddress().startsWith("$")) {//is IPAddress static
            if(isNotIpAddress(getIPAddress())){
                issues.add(getContext().createConfigIssue(Groups.COMMUNICATION.name(), MelsecConstants.COMM_GROUP_LABEL, Errors.ERROR_055, ""));
            }
            else{
                if(getNetworkConnectionType().equals(MelsecConstants.OPTION_VALUE_TCPIP) && !isRedundancy()) {
                    if (!checkConnectivity()) {
                        issues.add(getContext().createConfigIssue(Groups.COMMUNICATION.name(), MelsecConstants.IP_ADDR_LABEL, Errors.ERROR_010, checkConnectivity()));
                    }
                }
                initClient(getIPAddress(), port);

            }

        }
        if(isRedundancy()){
            addressPortList.add(getPort());
            if(!getRedundancyAddressList().isEmpty()){
                for(String item : getRedundancyAddressList()){
                    addressPortList.add(Integer.valueOf(item));
                }
            }
        }
        if(getInputDataTypeName().equals("")){
            issues.add(getContext().createConfigIssue(Groups.ADDRESS.name(), MelsecConstants.DATA_TYPE_LABEL, Errors.ERROR_052, ""));
        }
        if( getInputAddressName().equals("")){
            issues.add(getContext().createConfigIssue(Groups.ADDRESS.name(), MelsecConstants.DATA_TYPE_LABEL, Errors.ERROR_051, ""));
        }
        if(getInputTagName().equals("")){
            issues.add(getContext().createConfigIssue(Groups.ADDRESS.name(), MelsecConstants.DATA_TYPE_LABEL, Errors.ERROR_053, ""));
        }
        if(getIPAddress().equals("")){
            issues.add(getContext().createConfigIssue(Groups.ADDRESS.name(), MelsecConstants.DATA_TYPE_LABEL, Errors.ERROR_050, ""));
        }
        return issues;
    }

    /** {@inheritDoc} */
    @Override
    public void destroy() {
        for(String key :  clientMap.keySet()){ disconnectClient(key); }
        super.destroy();
    }
    private void disconnectClient(String ipAddress){
        if(clientMap.get(ipAddress)!=null){
            try {
                logger.info("disconnecting {}....", ipAddress);
                clientMap.get(ipAddress).disconnect().get(getAddr_Cycle_Timeout(), TimeUnit.MILLISECONDS);
            } catch (InterruptedException | ExecutionException e) {
                throw new StageException(Errors.ERROR_700, e.getMessage());
            }
            catch(TimeoutException ignored) {}
        }
    }
    /** {@inheritDoc} */
    @Override
    public void write(Batch batch) throws StageException {
        Iterator<Record> batchIterator = batch.getRecords();

        while (batchIterator.hasNext()) {
            Record record = batchIterator.next();
            try {
                write(record);
            } catch (Exception e) {
                switch (getContext().getOnErrorRecord()) {
                    case DISCARD:
                        break;
                    case TO_ERROR:
                        getContext().toError(record, e.getMessage());
                        break;
                    case STOP_PIPELINE:
                        throw new StageException(Errors.ERROR_150, e.getMessage());
                    default:
                        throw new IllegalStateException(
                                Utils.format("Unknown OnError value '{}'", getContext().getOnErrorRecord(), e)
                        );
                }
            }
        }
    }

    /**
     * Writes a single record to the destination.
     *
     * @param record the record to write to the destination.
     * @throws OnRecordErrorException when a record cannot be written.
     */
    private void write(Record record) throws OnRecordErrorException {
        if(getInputDataFormat().equals(MelsecConstants.OPTION_VALUE_MULTIPLE_JSON_FORMAT)) { sendSingleRecord(record); }
        else{ sendMultipleRecord((List<Map<String, Field>>) record.get().getValue());}
    }
    private void sendMultipleRecord(List<Map<String, Field>> list){
        Field value;
        String ipAddress;
        String dataType;
        String address;
        for (Map<String, Field> stringFieldMap : list) {
            if (getIPAddress().startsWith("$")) {
                ipAddress = stringFieldMap.get(getIPAddress()).getValueAsString();
                setClient(ipAddress);
            }
            else{  ipAddress = getIPAddress(); }
            address = stringFieldMap.get(getInputAddressName()).getValueAsString();
            dataType = stringFieldMap.get(getInputDataTypeName()).getValueAsString();
            value = stringFieldMap.get(getInputTagName());
            sendRecord(address, dataType, value, ipAddress);
        }
    }
    private void setClient(String ipAddress){
        if (!addressIndexMap.containsKey(ipAddress)) {
            int currentPort;
            if(isRedundancy()) { currentPort = addressPortList.get(0); }
            else { currentPort = port; }
            initClient(ipAddress, currentPort);
            addressIndexMap.put(ipAddress, currentPort);
        }
    }
    private void sendSingleRecord(Record record){
        Field value;
        String ipAddress=getIPAddress();
        String dataType;
        String address;
        if (ipAddress.startsWith("$")) {
            RecordEL.setRecordInContext(nameVars, record);
            ipAddress = nameEval.eval(nameVars, getIPAddress(), String.class);
            setClient(ipAddress);
        }
        try {
            value = record.get("/" + getInputTagName());
        } catch (Exception e) {
            throw new OnRecordErrorException(Errors.ERROR_154);
        }
        try {
            dataType = record.get("/" + getInputDataTypeName()).getValueAsString().toUpperCase();
        } catch (Exception e) {
            throw new OnRecordErrorException(Errors.ERROR_155, Errors.ERROR_155.getMessage());
        }
        try {
            address = record.get("/" + getInputAddressName()).getValueAsString();
        } catch (Exception e) {
            throw new OnRecordErrorException(Errors.ERROR_153);
        }
        sendRecord(address, dataType, value, ipAddress);
    }
    private void initClient(String ipAddress, int port){
        MelsecClient client;
        MelsecClientConfig config = new MelsecClientConfig.Builder(ipAddress)
                .setConnectionType(getNetworkConnectionType())
                .setNetworkNo(getPcNetworkNumber())
                .setPcNo(getPcStationNumber())
                .setRequestDestinationModuleStationNo(getMelsecCPULocation())
                .setRequestDestinationModuleStationNo(getMelsecStationNumber())
                .setTimeout(Duration.ofMillis(getAddr_Cycle_Timeout()))
                .setPort(port).build();
        if (getProtocolType().equals(MelsecConstants.OPTION_VALUE_BINARY)) { client = MelsecClient.create3EEthBinary(config);}
        else { client = MelsecClient.create3EEthAscii(config); }
        clientMap.put(ipAddress, client);
    }

    private void sendRecord(String address, @NotNull String dataType, Field value, String ipAddress){
        ByteBuf message;
        if(isNotIpAddress(ipAddress)){ throw new OnRecordErrorException(Errors.ERROR_152, ipAddress); }
        int writePoints = 1;
        switch (dataType) {
            case MelsecConstants.BOOLEAN_CASE:
                message = Unpooled.buffer(writePoints);
                message.writeBoolean(value.getValueAsBoolean());
                break;
            case MelsecConstants.UNSIGNED_INTEGER_CASE:
            case MelsecConstants.SIGNED_INTEGER_CASE:
                message = Unpooled.buffer(writePoints);
                message.writeShort(value.getValueAsShort());
                break;
            case MelsecConstants.STRING_CASE:
                message = Unpooled.buffer(writePoints);
                message.writeBytes(value.getValueAsByteArray());
                break;
            case MelsecConstants.WORD_CASE:
                message = Unpooled.buffer(writePoints);
                message.writeShort(value.getValueAsInteger());
                break;
            case MelsecConstants.FLOAT_CASE:
                writePoints = 2;
                message = Unpooled.buffer(writePoints);
                int inputValue = Float.floatToIntBits(value.getValueAsFloat());
                int loBits = inputValue >>>16;
                int hiBits = inputValue <<16;
                message.writeInt(hiBits+loBits);
                break;
            case MelsecConstants.DWORD_CASE:
                writePoints = 2;
                message = Unpooled.buffer(writePoints);
                int loWord = (value.getValueAsInteger() << 16) >>> 16;
                int hiWord = value.getValueAsInteger() >>> 16;
                message.writeShort(loWord);
                message.writeShort(hiWord);
                break;
            case MelsecConstants.LONG_CASE:
            case MelsecConstants.QWORD_CASE:
                writePoints = 4;
                message = Unpooled.buffer(writePoints);
                long qwordValue = value.getValueAsLong();
                long[] qWord = new long[4];
                qWord[0] = qwordValue << 48 >>> 48;
                qWord[1] = qwordValue << 32 >>> 48;
                qWord[2] = qwordValue << 16 >>> 48;
                qWord[3] = qwordValue >>> 48;
                for (int i = 0; i < 4; i++) {
                    message.writeShort((int) qWord[i]);
                }
                break;
            case MelsecConstants.DOUBLE_CASE:
                writePoints = 4;
                message = Unpooled.buffer(writePoints);
                long doubleValue = Double.doubleToLongBits(value.getValueAsDouble());
                long bit1 = doubleValue >>> 48;
                long bit2 = (doubleValue >>> 32) << 16;
                long bit3 = (doubleValue >>> 16) << 32;
                long bit4 = doubleValue << 48;
                message.writeLong(bit1+bit2+bit3+bit4);
                break;
            default:
                throw new OnRecordErrorException(Errors.ERROR_155);
        }
        try {
            clientMap.get(ipAddress).batchWrite(address, writePoints, message).get();

        } catch (ExecutionException e) {
            logger.warn("Sending Execution Exception occurred"+ e.toString());
            if(isRedundancy()) {
                disconnectClient(ipAddress);
                int port = cycleNextPort(ipAddress);
                initClient(ipAddress, port);
                throw new OnRecordErrorException(Errors.ERROR_105, port);
            }
            else {
                throw new OnRecordErrorException(Errors.ERROR_101, e.getMessage());
            }

        } catch (InterruptedException e) {
            throw new OnRecordErrorException(Errors.ERROR_100, e.getMessage());
        } finally {
           // message.release();
        }
    }

    private boolean isNotIpAddress(String address){
        boolean result = true;
        if(Pattern.matches(MelsecConstants.IP_PATTERN, address)||address.equalsIgnoreCase("localhost")){
            result = false;
        }
        return result;
    }
    private boolean checkConnectivity() throws StageException{
        Socket socket = null;
        try {
            socket = new Socket(getIPAddress(), getPort());
            socket.setSoTimeout(getAddr_Cycle_Timeout());
            socket.close();
        }
        catch (UnknownHostException | SocketException e) { return false; }
        catch (IOException e) {
            try {
                assert socket != null;
                socket.close(); }
            catch (IOException e1) { e1.printStackTrace(); }
        }
        return true;
    }
}
