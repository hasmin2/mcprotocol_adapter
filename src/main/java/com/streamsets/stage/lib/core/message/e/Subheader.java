package com.streamsets.stage.lib.core.message.e;


import com.streamsets.stage.lib.core.message.BinaryCodec;

/**
 * 副头部
 *
 * @author liumin
 */
public interface Subheader extends BinaryCodec {

}
