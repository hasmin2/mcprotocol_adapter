package com.streamsets.stage.lib.menus;

import com.streamsets.pipeline.api.base.BaseEnumChooserValues;

public class InputDataFormatChooserValues extends BaseEnumChooserValues<InputDataFormat> {
    public InputDataFormatChooserValues() {
        super(InputDataFormat.class);
    }
}

