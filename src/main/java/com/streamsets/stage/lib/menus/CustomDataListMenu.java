package com.streamsets.stage.lib.menus;

import com.streamsets.pipeline.api.ConfigDef;
import com.streamsets.pipeline.api.ValueChooserModel;
import com.streamsets.stage.lib.MelsecConstants;

public class CustomDataListMenu {
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.STRING,
            label = MelsecConstants.START_ADDRESS_LABEL,
            defaultValue = "X000",
            description = MelsecConstants.START_ADDRESS_DESC
    )
    public String startAddress;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            label = MelsecConstants.READ_POINT_LABEL,
            defaultValue = "1",
            min = 1,
            max = MelsecConstants.READ_POINT_LIMIT,
            description = MelsecConstants.READ_POINT_DESC
    )
    public int points;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            label = MelsecConstants.DATA_TYPE_LABEL,
            defaultValue = "BOOLEAN",
            description = MelsecConstants.MELSEC_DATA_TYPE_DESC
    )
    @ValueChooserModel(OutputDataTypeChooserValues.class)
    public OutputDataType dataType;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            label = MelsecConstants.SCALE_LABEL,
            defaultValue = "10000",
            min = 0,
            max = 10000,
            dependsOn = "dataType",
            triggeredByValue = {MelsecConstants.WORD_CASE, MelsecConstants.DWORD_CASE,
                    MelsecConstants.QWORD_CASE, MelsecConstants.UNSIGNED_INTEGER_CASE, MelsecConstants.SIGNED_INTEGER_CASE},
            description = MelsecConstants.SCALE_DESC
    )
    public double scale;
}