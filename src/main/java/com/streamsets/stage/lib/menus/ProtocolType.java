package com.streamsets.stage.lib.menus;

import com.streamsets.pipeline.api.Label;
import com.streamsets.stage.lib.MelsecConstants;

public enum ProtocolType implements Label {
    ASCII(MelsecConstants.OPTION_VALUE_ASCII),
    BINARY(MelsecConstants.OPTION_VALUE_BINARY);

    private final String label;

    ProtocolType (String label) {
        this.label = label;
    }

    @Override
    public String getLabel() {
        return label;
    }
}
