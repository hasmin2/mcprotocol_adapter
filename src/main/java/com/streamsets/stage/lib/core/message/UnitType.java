package com.streamsets.stage.lib.core.message;

/**
 * @author liumin
 */
public enum UnitType {

    BIT,
    WORD
}
