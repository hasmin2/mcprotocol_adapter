package com.streamsets.stage.lib.menus;

import com.streamsets.pipeline.api.base.BaseEnumChooserValues;

public class MelsecCPULocationChooserValues extends BaseEnumChooserValues<MelsecCPULocation> {
    public MelsecCPULocationChooserValues() {
        super(MelsecCPULocation.class);
    }

}
