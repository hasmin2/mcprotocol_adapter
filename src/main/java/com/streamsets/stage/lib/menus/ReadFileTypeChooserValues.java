package com.streamsets.stage.lib.menus;

import com.streamsets.pipeline.api.base.BaseEnumChooserValues;

public class ReadFileTypeChooserValues extends BaseEnumChooserValues<ReadFileType> {
    public ReadFileTypeChooserValues() {
        super(ReadFileType.class);
    }
}
