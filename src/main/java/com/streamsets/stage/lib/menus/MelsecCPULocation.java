package com.streamsets.stage.lib.menus;

import com.streamsets.pipeline.api.GenerateResourceBundle;
import com.streamsets.pipeline.api.Label;
import com.streamsets.stage.lib.MelsecConstants;

@GenerateResourceBundle
public enum MelsecCPULocation implements Label {
    CPULOCAL(MelsecConstants.OPTION_VALUE_CPU_LOCAL),
    CPUCONTROL(MelsecConstants.OPTION_VALUE_CPU_CONTROL),
    CPUNO1(MelsecConstants.OPTION_VALUE_CPU_NO1),
    CPUNO2(MelsecConstants.OPTION_VALUE_CPU_NO2),
    CPUNO3(MelsecConstants.OPTION_VALUE_CPU_NO3),
    CPUNO4(MelsecConstants.OPTION_VALUE_CPU_NO4),
    CPUSTANDBY(MelsecConstants.OPTION_VALUE_CPU_STANDBY),
    CPUSYSTEMA(MelsecConstants.OPTION_VALUE_CPU_SYSTEM_A),
    CPUSYSTEMB(MelsecConstants.OPTION_VALUE_CPU_SYSTEM_B);
    private final String label;

    MelsecCPULocation(String label) {
        this.label = label;
    }

    @Override
    public String getLabel() {
        return label;
    }
}
