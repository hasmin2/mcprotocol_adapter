package com.streamsets.stage.lib.core.message.e;

import com.streamsets.stage.lib.core.message.e.qheader.AbstractResponseQHeader;
import com.streamsets.stage.lib.core.message.e.qheader.BinaryErrorInformationSection;
import com.streamsets.stage.lib.core.message.e.qheader.BinaryResponseQHeader;
import com.streamsets.stage.lib.core.message.e.qheader.ErrorInformationSection;
import com.streamsets.stage.lib.core.message.e.subheader.Frame3EBinaryResponseSubheader;
import io.netty.buffer.ByteBuf;

/**
 * @author liumin
 */
public class Frame3EBinaryResponse extends AbstractFrameEResponse {

    private ByteBuf data;

    private ErrorInformationSection errorInformationSection;

    @Override
    protected Subheader newSubheader() {
        return Frame3EBinaryResponseSubheader.getInstance();
    }

    @Override
    protected AbstractResponseQHeader newQHeader() {
        return new BinaryResponseQHeader();
    }

    @Override
    public ByteBuf getData() {
        return data;
    }

    @Override
    public void setData(ByteBuf data) {
        this.data = data;
    }

    @Override
    public ErrorInformationSection getErrorInformationSection() {
        if (errorInformationSection == null) {
            errorInformationSection = new BinaryErrorInformationSection();
        }
        return errorInformationSection;
    }

    @Override
    public String toString() {
        return "Frame3EBinaryResponse{" +
                "qHeader=" + getQHeader() +
                ", data=" + (getData()) +
                ", errorInformationSection=" + errorInformationSection +
                '}';
    }
}
