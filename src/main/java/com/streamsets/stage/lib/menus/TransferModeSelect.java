package com.streamsets.stage.lib.menus;

import com.streamsets.pipeline.api.Label;
import com.streamsets.stage.lib.MelsecConstants;

public enum TransferModeSelect implements Label {
    ONLYCHANGE(MelsecConstants.OPTION_VALUE_ONLY_CHANGE),
    WHILEONLINE(MelsecConstants.OPTION_VALUE_WHILE_ONLINE_ONLY),
    TRANSFERALWAYS(MelsecConstants.OPTION_VALUE_TRANSFER_ALWAYS);
    private final String label;

    TransferModeSelect(String label) { this.label = label; }

    @Override
    public String getLabel() {
        return label;
    }
}
