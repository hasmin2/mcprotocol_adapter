package com.streamsets.stage.lib.menus;

import com.streamsets.pipeline.api.Label;
import com.streamsets.stage.lib.MelsecConstants;

public enum NetworkConnectionType implements Label {
    UDPTYPE(MelsecConstants.OPTION_VALUE_UDP),
    TCPIPTYPE(MelsecConstants.OPTION_VALUE_TCPIP);
    private final String label;

    NetworkConnectionType(String label) {
        this.label = label;
    }

    @Override
    public String getLabel() {
        return label;
    }
}
