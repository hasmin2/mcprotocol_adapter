package com.streamsets.stage.lib.util;

import com.streamsets.stage.lib.menus.OutputDataType;

public class AddressListStore {
    private String address;
    private int points;
    private OutputDataType dataType;
    private double scale;

    public String getAddress(){ return address;}
    public int getPoints() { return points; }
    public double getScale() { return scale; }
    public OutputDataType getDataType() { return dataType; }
    public void setDataType(OutputDataType dataType) { this.dataType = dataType;}
    public void setAddress(String address) { this.address = address;}
    public void setPoints(int points) { this.points = points; }
    public void setScale(double scale) { this.scale = scale; }

}
