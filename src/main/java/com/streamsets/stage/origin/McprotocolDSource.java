/**
 * Copyright 2015 StreamSets Inc.
 * <p>
 * Licensed under the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.streamsets.stage.origin;

import com.streamsets.pipeline.api.*;
import com.streamsets.stage.lib.MelsecConstants;
import com.streamsets.stage.lib.menus.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@StageDef(
        version = 1,
        label = MelsecConstants.MELSEC_ORIGIN_LABEL,
        description = MelsecConstants.MELSEC_ORIGIN_DESC,
        icon = MelsecConstants.MELSEC_ICON_NAME,
        execution = ExecutionMode.STANDALONE,
        recordsByRef = true,
        onlineHelpRefUrl = ""
)
@ConfigGroups(value = Groups.class)
@GenerateResourceBundle
public class McprotocolDSource extends McprotocolSource {
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            defaultValue = MelsecConstants.NETTYPE_ETH_OPTION,
            label = MelsecConstants.COMMTYPE_LABEL,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP,
            description = MelsecConstants.COMMTYPE_DESC
    )
    @ValueChooserModel(NetworkTypeChooserValues.class)
    public NetworkType commType;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.STRING,
            defaultValue = MelsecConstants.IP_VALUE,
            label = MelsecConstants.IP_ADDR_LABEL,
            dependsOn = "commType",
            triggeredByValue = MelsecConstants.NETTYPE_ETH_OPTION,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP,
            description = MelsecConstants.IP_ADDR_DESC
    )
    public String ipAddress;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            defaultValue = "ONLYCHANGE",
            label = MelsecConstants.TRANSFER_MODE_LABEL,
            description = MelsecConstants.TRANSFER_MODE_DESC,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP
    )
    @ValueChooserModel(TransferModeSelectChooserValues.class)
    public TransferModeSelect transferMode;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            defaultValue = "INTEGERTYPE",
            label = MelsecConstants.BOOLEAN_OUTPUT_LABEL,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP,
            description = MelsecConstants.BOOLEAN_OUTPUT_DESC
    )
    @ValueChooserModel(BooleanOutPutTypeChooserValues.class)
    public BooleanOutPutType booleanOutputType;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = MelsecConstants.SCANRATE_VALUE,
            label = MelsecConstants.SCANRATE_LABEL,
            description = MelsecConstants.SCANRATE_DESC,
            min=15,
            max=86400000,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP
    )
    public int scanRate;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = "0",
            label = MelsecConstants.PC_NETWORK_NO_LABEL,
            description = MelsecConstants.PC_NETWORK_NO_DESC,
            min=0,
            max=255,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP
    )
    public int pcNetworkNumber;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = "255",
            label = MelsecConstants.PC_STATION_NO_LABEL,
            description = MelsecConstants.PC_STATION_NO_DESC,
            min=0,
            max=255,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP
    )
    public int pcStationNumber;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            defaultValue = "CPULOCAL",
            label = MelsecConstants.MELSEC_CPULOCATION_LABEL,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP,
            description = MelsecConstants.MELSEC_CPU_LOCATION_DESC
    )
    @ValueChooserModel(MelsecCPULocationChooserValues.class)
    public MelsecCPULocation cpuLocation;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = "0",
            label = MelsecConstants.MELSEC_STATION_NO_LABEL,
            description = MelsecConstants.MELSEC_STATION_NO_DESC,
            min=0,
            max=31,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP
    )
    public int melsecStationNumber;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = MelsecConstants.PORT_VALUE,
            label = MelsecConstants.PORT_LABEL,
            dependsOn = "commType",
            triggeredByValue = MelsecConstants.NETTYPE_ETH_OPTION,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP,
            description = MelsecConstants.PORT_DESC
    )
    public int port;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.STRING,
            defaultValue = MelsecConstants.COMPORT_VALUE,
            label = MelsecConstants.COMPORT_LABEL,
            dependsOn = "commType",
            triggeredByValue = MelsecConstants.NETTYPE_SER_OPTION,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP,
            description = MelsecConstants.COMPORT_DESC
    )
    public String comPort;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            defaultValue = "",
            label = MelsecConstants.NETWORK_CONNECTION_TYPE_LABEL,
            description = MelsecConstants.NETWORK_CONNECTION_TYPE_DESC,
            displayPosition = 10,
            group = MelsecConstants.COMM_GROUP
    )
    @ValueChooserModel(NetworkConnectionTypeChooserValues.class)
    public NetworkConnectionType networkConnectionType;

    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            defaultValue = MelsecConstants.LIST_FROM_UI_OPTION,
            label = MelsecConstants.DATA_MANAGE_SELECTION_LABEL,
            description = MelsecConstants.DATA_MANAGE_SELECTION_DESC,
            displayPosition = 10,
            group = MelsecConstants.ADDRESS_GROUP
    )
    @ValueChooserModel(ReadFileTypeChooserValues.class)
    public ReadFileType customDataListFromCsv;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.STRING,
            defaultValue = MelsecConstants.FILEPATH_VALUE,
            label = MelsecConstants.FILEPATH_LABEL,
            dependsOn = "customDataListFromCsv",
            triggeredByValue = {MelsecConstants.KEPWARE_CSV_OPTION},
            displayPosition = 10,
            group = MelsecConstants.ADDRESS_GROUP,
            description = MelsecConstants.FILEPATH_DESC
    )
    public String filePathName;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            label = "",
            description = "",
            dependsOn = "customDataListFromCsv",
            triggeredByValue = MelsecConstants.LIST_FROM_UI_OPTION,
            displayPosition = 210,
            group = MelsecConstants.ADDRESS_GROUP
    )
    @ListBeanModel
    public List<CustomDataListMenu> customDataListMenus;
    @ConfigDef(
            required = false,
            type = ConfigDef.Type.MAP,
            defaultValue = "",
            label = MelsecConstants.REPLY_MSG_LABEL,
            displayPosition = 220,
            group = MelsecConstants.ADDRESS_GROUP,
            description = MelsecConstants.REPLY_MSG_DESC
    )
    public Map<String, String> replyValueMap = new HashMap<>();
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.MODEL,
            label = MelsecConstants.PROTOCOL_TYPE_LABEL,
            description = MelsecConstants.PROTOCOL_TYPE_DESC,
            displayPosition = 210,
            group = MelsecConstants.COMM_GROUP
    )
    @ValueChooserModel(ProtocolTypeChooserValues.class)
    public ProtocolType protocolType;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.BOOLEAN,
            defaultValue = "FALSE",
            label = MelsecConstants.REDUNDANCY_ENABLE_LABEL,
            description = MelsecConstants.REDUNDANCY_ENABLE_DESC,
            displayPosition = 1,
            dependsOn = "transferMode",
            triggeredByValue = {"ONLYCHANGE","WHILEONLINE"},
            group = MelsecConstants.REDUNDANCY_GROUP
    )
    public boolean redundancyEnabled;
    @ConfigDef(
            required = false,
            type = ConfigDef.Type.LIST,
            label = MelsecConstants.REDUNDANCY_ADDRLIST_LABEL,
            description = MelsecConstants.REDUNDANCY_ADDRLIST_DESC,
            displayPosition = 10,
            group = MelsecConstants.REDUNDANCY_GROUP,
            dependsOn = "redundancyEnabled",
            triggeredByValue = "true"
    )
    @ListBeanModel
    public List<String> redundancyAddressList;

    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = "6000",
            label = MelsecConstants.ADDR_CYCLE_TIME_LABEL,
            description = MelsecConstants.ADDR_CYCLE_TIME_DESC,
            min=1000,
            max=60000,
            displayPosition = 20,
            group = MelsecConstants.COMM_GROUP
    )
    public int addr_Cycle_Timeout;
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRedundancy() { return redundancyEnabled; }
    @Override
    public int getAddr_Cycle_Timeout() { return addr_Cycle_Timeout; }
    @Override
    public List<String> getRedundancyAddressList() {return redundancyAddressList; }
    @Override
    public Map<String,String>getReplyValueMap() { return replyValueMap; }
    @Override
    public String getNetworkType() {return commType.getLabel();}
    @Override
    public String getIPAddress() {return ipAddress;}
    @Override
    public int getPort() {return port;}
    @Override
    public String getComPort() {return comPort;}
    @Override
    public String getFilePathName() {return filePathName;}
    @Override
    public int getScanRate() {return scanRate;}
    @Override
    public TransferModeSelect getTransferMode() {return transferMode;}
    @Override
    public String getCustomDataListFromCsv() {return customDataListFromCsv.getLabel();}
    @Override
    public String getBooleanOutputType() {return booleanOutputType.getLabel();}
    @Override
    public List<CustomDataListMenu> getCustomDataListMenus() {return customDataListMenus;}
    //네트워 크연 결타
    @Override
    public String getNetworkConnectionType() {return networkConnectionType.getLabel();}

    //PC 쪽에 서지정하 는plc 번 호리턴
    @Override
    public int getPcNetworkNumber() {return pcNetworkNumber;}
    //PC 쪽에 서지정하 는plc 번 호리턴
    @Override
    public int getPcStationNumber() {
        int result;
        if(pcNetworkNumber==0) {result = 255;}
        else { result = pcStationNumber; }
        return result ;
    }
    @Override
    public int getMelsecCPULocation() {
        int result;
        switch (cpuLocation.name()) {
            case "CPULOCAL":
                result = 0x03FF;
                break;
            case "CPUNO1":
                result = 0x03E0;
                break;
            case "CPUNO2":
                result = 0x03E1;
                break;
            case "CPUNO3":
                result = 0x03E2;
                break;
            case "CPUNO4":
                result = 0x03E3;
                break;
            default:
                result = 0;
        }
        return result;
    }
    @Override
    public int getMelsecStationNumber(){ return melsecStationNumber;}
    @Override
    public String getProtocolType(){ return protocolType.getLabel();}
}
