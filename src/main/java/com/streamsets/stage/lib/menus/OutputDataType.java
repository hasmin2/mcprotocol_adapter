package com.streamsets.stage.lib.menus;

import com.streamsets.pipeline.api.Label;
import com.streamsets.stage.lib.MelsecConstants;

public enum OutputDataType implements Label {
    BOOLEAN(MelsecConstants.OPTION_VALUE_BOOLEAN),
    WORD(MelsecConstants.OPTION_VALUE_WORD),
    DWORD(MelsecConstants.OPTION_VALUE_DWORD),
    QWORD(MelsecConstants.OPTION_VALUE_QWORD),
    STRING(MelsecConstants.OPTION_VALUE_STRING),
    UNSIGNED_INTEGER(MelsecConstants.OPTION_VALUE_UNSIGNED_INTEGER),
    SIGNED_INTEGER(MelsecConstants.OPTION_VALUE_SIGNED_INTEGER),
    LONG(MelsecConstants.OPTION_VALUE_LONG),
    FLOAT(MelsecConstants.OPTION_VALUE_FLOAT),
    DOUBLE(MelsecConstants.OPTION_VALUE_DOUBLE);
    private final String label;

    OutputDataType(String label) {
        this.label = label;
    }
    @Override
    public String getLabel() {
        return label;
    }
}
