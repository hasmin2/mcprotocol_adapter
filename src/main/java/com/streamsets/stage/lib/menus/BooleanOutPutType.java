package com.streamsets.stage.lib.menus;

import com.streamsets.pipeline.api.Label;
import com.streamsets.stage.lib.MelsecConstants;

public enum BooleanOutPutType implements Label {
    INTEGERTYPE(MelsecConstants.OPTION_VALUE_INTEGER_TYPE),
    BOOLEANTYPE(MelsecConstants.OPTION_VALUE_BOOLEAN_TYPE);
    private final String label;

    BooleanOutPutType(String label) {
        this.label = label;
    }

    @Override
    public String getLabel() {
        return label;
    }
}
