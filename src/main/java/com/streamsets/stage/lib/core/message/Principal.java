package com.streamsets.stage.lib.core.message;

import io.netty.buffer.ByteBuf;

/**
 * @author liumin
 */
public interface Principal extends BinaryCodec {

    String getAddress();

    void setAddress(String address);

    String getRealAddress();

    void setRealAddress(String realAddress);

    int getPoints();

    void setPoints(int points);

    Device getDevice();

    void setDevice(Device device);

    Function getFunction();

    void setFunction(Function function);

    int getSubcommand();

    void setSubcommand(int subcommand);

    ByteBuf getData();

    void setData(ByteBuf data);
}
