/**
 * Copyright 2015 StreamSets Inc.
 * <p>
 * Licensed under the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.streamsets.stage.origin;

import com.streamsets.pipeline.api.Record;
import com.streamsets.pipeline.sdk.SourceRunner;
import com.streamsets.pipeline.sdk.StageRunner;
import com.streamsets.stage.lib.menus.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestMcprotocolSource {
    private static final int MAX_BATCH_SIZE = 5;

    @Test
    public void testOrigin() throws Exception {
        List<CustomDataListMenu> customDataListMenuList = new ArrayList<>();
        CustomDataListMenu customDataListMenu = new CustomDataListMenu();
        customDataListMenu.startAddress = "X000";
        //customDataListMenu.startAddress = "X000";
        customDataListMenu.scale = 10000;
        customDataListMenu.points = 1;
        customDataListMenu.dataType = OutputDataType.BOOLEAN;
        //customDataListMenu.dataType = OutputDataType.BOOLEAN;
        customDataListMenuList.add(customDataListMenu);
        Map<String, String> replyValueMap = new HashMap<>();
        List<String> alterAddress = new ArrayList<>();
        alterAddress.add("5001");
        alterAddress.add("5002");

        SourceRunner runner = new SourceRunner.Builder(McprotocolDSource.class)
                .addConfiguration("melsecStationNumber", 0)
                .addConfiguration("protocolType", ProtocolType.BINARY)
                .addConfiguration("cpuLocation", MelsecCPULocation.CPULOCAL)
                .addConfiguration("pcStationNumber", 255)
                .addConfiguration("pcNetworkNumber", 0)
                .addConfiguration("networkConnectionType", NetworkConnectionType.UDPTYPE)
                .addConfiguration("ipAddress", "10.7.10.178")
                .addConfiguration("port", 5000)
                .addConfiguration("booleanOutputType", BooleanOutPutType.INTEGERTYPE)
                .addConfiguration("commType", NetworkType.ETHERNET)
                .addConfiguration("transferMode", TransferModeSelect.ONLYCHANGE)
                .addConfiguration("scanRate", 1000)
                .addConfiguration("filePathName", "/data/home/hhiroot/Documents/FIT_UP1_CORNER_FACTORY1.csv")
                //.addConfiguration("customDataListFromCsv", ReadFileType.KEPWARECSV)
                .addConfiguration("customDataListFromCsv", ReadFileType.LISTFROMUI)
                .addConfiguration("customDataListMenus", customDataListMenuList)
                .addConfiguration("replyValueMap", replyValueMap)
                .addConfiguration("redundancyEnabled", false)
                .addConfiguration("addr_Cycle_Timeout", 6000)
                .addConfiguration("redundancyAddressList", alterAddress)
                .addOutputLane("lane")
                .build();
            long beginTime = System.currentTimeMillis();
            try {
                runner.runInit();
                final String lastSourceOffset = null;
                StageRunner.Output output = runner.runProduce(lastSourceOffset, MAX_BATCH_SIZE);
                //Assert.assertEquals("2", output.getNewOffset());
                List<Record> records = output.getRecords().get("lane");
                //records.forEach(record -> System.out.println("Address:" + record.get("/address").getValueAsString()+ " , Value:" + record.get("/value").getValue()));
                System.out.println("Record Size is :::"+records.size());
                long timeGap = System.currentTimeMillis() - beginTime;
                System.out.println("Time elasped is : " + timeGap);
                //Assert.assertTrue(records.get(0).has("/value"));

                //Assert.assertEquals("/value", records.get(0).get("/adderss").getValueAsString());

            } finally {
                runner.runDestroy();
            }

    }



}
