package com.streamsets.stage.lib.menus;

import com.streamsets.pipeline.api.base.BaseEnumChooserValues;

public class ProtocolTypeChooserValues extends BaseEnumChooserValues<ProtocolType> {
    public ProtocolTypeChooserValues() { super(ProtocolType.class);}
}
